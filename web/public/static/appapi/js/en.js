//例： '你好，{n}':'hello,{n}'
var language_pack={
    '上传失败':'Fail to upload',
    '提交成功':'submitted successfully',
    /* Auth */
    '请输入姓名':'Please type in name',
    '请输入手机号码':'Please type in your mobile phone No.',
    '请输入身份证号码':'Please type in ID No.',
    '请上传证件正面':'Please submit the front of the certificate',
    '请上传证件反面':'Please submit the reverse side of the certificate',
    '请上传手持证件正面照':'Please upload the the photo holding the front of the certificate',
    
    /* Skillauth */
    '请上传截图':'Please upload screenshots',
    '请选择段位':'Please select dan grading',
    '确定':'OK',
    '取消':'Cancel',
};