<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------

namespace app\user\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

/**
 * Class AdminIndexController
 * @package app\user\controller
 *
 * @adminMenuRoot(
 *     'name'   =>'用户管理',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 10,
 *     'icon'   =>'group',
 *     'remark' =>'用户管理'
 * )
 *
 * @adminMenuRoot(
 *     'name'   =>'用户组',
 *     'action' =>'default1',
 *     'parent' =>'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   =>'',
 *     'remark' =>'用户组'
 * )
 */
class AdminIndexController extends AdminBaseController
{

    /**
     * 后台本站用户列表
     * @adminMenu(
     *     'name'   => '本站用户',
     *     'parent' => 'default1',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户',
     *     'param'  => ''
     * )
     */
    public function index(){
		
		$data = $this->request->param();
        $map=[];
		$map[]=['user_type','=',2];
		
		$uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
			$map[]=['id','=',$uid];
        }
		
		
		$start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['create_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['create_time','<=',strtotime($end_time) + 60*60*24];
        }
		
		$issuper=isset($data['issuper']) ? $data['issuper']: '';
        if($issuper!=''){
            $map[]=['issuper','=',$issuper];
        }
		
		$ishost=isset($data['ishost']) ? $data['ishost']: '';
        if($ishost!=''){
            $map[]=['ishost','=',$ishost];
        }
		
		$source=isset($data['source']) ? $data['source']: '';
        if($source!=''){
            $map[]=['source','=',$source];
        }
		
		$keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['user_login|user_nickname|mobile','like','%'.$keyword.'%'];
        }
		
        $content = hook_one('user_admin_index_view');

        if (!empty($content)) {
            return $content;
        }

        $list = Db::name('user')
			->where($map)
            ->order("create_time DESC")
            ->paginate(20);
			
			
		$nums=Db::name("user")
				->where($map)
				->count();
        // 获取分页显示
        $list->each(function($v,$k){
            $v['user_login']=m_s($v['user_login']);
            // $v['user_email']=m_s($v['user_email']);
            $v['mobile']=m_s($v['mobile']);
            return $v;
        });
		
		$list->appends($data);
        $page = $list->render();
        $this->assign('list', $list);
        $this->assign('page', $page);
		$this->assign('nums', $nums);
        // 渲染模板输出
        return $this->fetch();
    }

    /**
     * 本站用户拉黑
     * @adminMenu(
     *     'name'   => '本站用户拉黑',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户拉黑',
     *     'param'  => ''
     * )
     */
    public function ban()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            $result = Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 0);
            if ($result) {
                $this->success("会员拉黑成功！", "adminIndex/index");
            } else {
                $this->error('会员拉黑失败,会员不存在,或者是管理员！');
            }
        } else {
            $this->error('数据传入失败！');
        }
    }

    /**
     * 本站用户启用
     * @adminMenu(
     *     'name'   => '本站用户启用',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '本站用户启用',
     *     'param'  => ''
     * )
     */
    public function cancelBan()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            Db::name("user")->where(["id" => $id, "user_type" => 2])->setField('user_status', 1);
            $this->success("会员启用成功！", '');
        } else {
            $this->error('数据传入失败！');
        }
    }

    /* 删除用户 */
    public function del()
    {
        $id = input('param.id', 0, 'intval');
        if ($id) {
            Db::name("user")->where(["id" => $id])->delete();
            
            /* 删除用户认证 */
            Db::name("user_auth")->where(["uid" => $id])->delete();
            
            /* 删除技能认证 */
            Db::name("skill_auth")->where(["uid" => $id])->delete();
            
            /* 粉丝、关注 */
            Db::name("user_attention")->where("uid={$id} or touid={$id}")->delete();
            
            /* token */
            Db::name("user_token")->where(["user_id" => $id])->delete();
            
            /* 评价 */
            Db::name("user_comment")->where(["touid" => $id])->delete();
            
            /* 技能标签 */
            Db::name("label_count")->where(["uid" => $id])->delete();
            
            $this->success("删除会员成功！", '');
        } else {
            $this->error('数据传入失败！');
        }
    }
    
    public function setHost()
    {
        $id = $this->request->param('id', 0, 'intval');
        if(!$id){
            $this->error("数据传入失败！");
        }
        $ishost = $this->request->param('ishost', 0, 'intval');
        
        $nowtime=time();
        
        $rs=DB::name("user")->where("id={$id}")->update(['ishost'=>$ishost]);
        if($rs===false){
            $this->error("操作失败");
        }
        
        $this->success("操作成功");        
    }
	/* 推荐 */
    function setrecommend(){
        
        $id = $this->request->param('id', 0, 'intval');
        $isrecommend = $this->request->param('isrecommend', 0, 'intval');
		
		$data=[
			'isrecommend'=>$isrecommend,
			'recommend_time'=>time(),
		];
		
		if($isrecommend==0){
			$data['recommend_time']='0';
		}
		$rs = DB::name('user')->where("id={$id}")->update($data);
		
		if(!$rs){
			$this->error("操作失败！");
		}
		DB::name('zlive')->where("uid={$id}")->update($data);
        $this->success("操作成功！");
            
	}
	function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('user')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['user_login']=m_s($data['user_login']);
        $this->assign('data', $data);
        return $this->fetch();
	}
	
	function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            //获取用户的状态
            $user_status=Db::name("user")->where("id={$data['id']}")->value("user_status");

			$user_nickname=$data['user_nickname'];
			if($user_nickname==""){
				$this->error("请填写昵称");
			}

            if($user_status!=3){
                if(strstr($user_nickname,'已注销')!==false){
                    $this->error("非注销用户昵称不能包含已注销");
                }  
            }

            if(mb_substr($user_nickname, 0,1)=="="){
                $this->error("昵称内容非法");
            }

			$avatar=$data['avatar'];
			$avatar_thumb=$data['avatar_thumb'];
			if( ($avatar=="" || $avatar_thumb=='' ) && ($avatar!="" || $avatar_thumb!='' )){
                $this->error("请同时上传头像 和 头像小图  或 都不上传");
			}
			
            $avatar_thumb_old=$data['avatar_thumb_old'];
			$avatar_old=$data['avatar_old'];
			
			if($avatar_old!=$data['avatar']){
				$data['avatar']=set_upload_path($data['avatar']);
			}
			if($avatar_thumb_old!=$data['avatar_thumb']){
				$data['avatar_thumb']=set_upload_path($data['avatar_thumb']);
			}
			
            if($avatar=='' && $avatar_thumb==''){
                $data['avatar']='/default.jpg';
                $data['avatar_thumb']='/default_thumb.jpg';
            }
            
			unset($data['avatar_old']);
			unset($data['avatar_thumb_old']);						
			$rs = DB::name('user')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            //查询用户信息存入缓存中
            $info=Db::name("user")
                        ->field('id,user_nickname,avatar,avatar_thumb,sex,signature,birthday,profession,school,hobby,voice,voice_l,addr,stars,star_nums,user_status,orders')
                        ->where("id={$data['id']} and user_type=2")
                        ->find();
            if($info){
                setcaches("userinfo_".$data['id'],$info);
            }
            
            $this->success("修改成功！");
		}
	}
	//标签管理
	function labelindex(){
		$id   = $this->request->param('id', 0, 'intval');
        $userlabel=Db::name('label_user')
			->where("uid={$id}")
            ->find();
        $labellist=Db::name('label_class')
            ->select();
        if(!$labellist){
            $this->error("信息错误");
        }
	
        
        $this->assign('userlabel', $userlabel);
        $this->assign('uid', $id);

        $this->assign('labellist', $labellist);
        return $this->fetch();
	}
	function editlabelPost(){
		if ($this->request->isPost()) {
            $data = $this->request->param();
            $uid = $data['uid'];
            $id = $data['id'];
            $label_id = $data['label_id'];
			if($label_id==0){
				DB::name('label_user')->where("uid={$uid}")->delete();
				$this->success("操作成功！");
			}
			
			
			$data['addtime']=time();
			$isexit=DB::name('label_user')->where("uid={$uid}")->find();
			if($isexit){
				$rs = DB::name('label_user')->update($data);
			}else{
				unset($data['id']);
				$rs = DB::name('label_user')->insert($data);
			}
			
            if($rs===false){
                $this->error("操作失败！");
            }
            $this->success("操作成功！");
		}
	}
	
	/* 超管 */
    function setsuper(){
        
        $id = $this->request->param('id', 0, 'intval');
        $issuper = $this->request->param('issuper', 0, 'intval');
        
        $rs = DB::name('user')->where("id={$id}")->setField('issuper',$issuper);
        if(!$rs){
            $this->error("操作失败！");
        }
        if($issuper==1){     
            $isexist=DB::name("user_super")->where("uid={$id}")->find();
            if(!$isexist){
                DB::name("user_super")->insert(array("uid"=>$id,'addtime'=>time()));	
            }
            
            hSet('super',$id,'1');
        }else{ //取消超管

            DB::name("user_super")->where("uid='{$id}'")->delete();
            hDel('super',$id);
        }
        $this->success("操作成功！");
            
	}
	
	
	
}
