<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
/**
 * 收益明细
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class DetailController extends HomebaseController {
	
	function index(){       
		$data = $this->request->param();
        $uid=isset($data['uid']) ? $data['uid']: '';
        $token=isset($data['token']) ? $data['token']: '';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        
		$this->assign("uid",$uid);
		$this->assign("token",$token);

		//收益明细
		
		$list_coin=Db::name('user_votesrecord')->where("uid={$uid} ")->order("addtime desc")->limit(0,50)->select()->toArray();

		// var_dump(Db::name('user_votesrecord')->getLastsql());
		foreach($list_coin as $k=>$v){
			$record_thumb="";
			$record_name="";
			$record_typename="";//消费类型：下单
			$action =$v['action'];
			if($action=='1'){
				$record_typename="接单";
			}
			
				$orderinfo=Db::name("orders")->field("skillid")->where("id='{$v['actionid']}'")->find();

				//$orderinfo=Db::name("orders")->field("skillid")->where("$action=1")->find();
				if($orderinfo){
					$skillinfo=Db::name("skill")->field("thumb,name")->where("id='{$orderinfo['skillid']}'")->find();
					if(!$skillinfo){
						$skillinfo=array(
							"name"=>'技能已删除',
							"thumb"=>"/default.png"
						);
					}
					$record_thumb=get_upload_path($skillinfo['thumb']);
					$record_name=$skillinfo['name'];
				}
			
			$list_coin[$k]['record_thumb']=$record_thumb;
			$list_coin[$k]['record_name']=$record_name;
			$list_coin[$k]['record_typename']=$record_typename;
			$userinfo=getUserInfo($v['fromid']);
			
			if(!$userinfo){
				$userinfo=array(
					"user_nicename"=>'用户已删除'
				);
			}
			$list_coin[$k]['userinfo']=$userinfo;
		}
		$list_coin=array_values($list_coin);
		
		$this->assign("list_coin",$list_coin);
		
		return $this->fetch();
	    
	}
	function subtext($text, $length){
		if(mb_strlen($text, 'utf8') > $length) {
			return mb_substr($text, 0, $length, 'utf8').'...';
		} else {
			return $text;
		}
	 
	}

	
	//更多收益记录
	public function coinlist_more()
	{
		$data = $this->request->param();
        $uid=isset($data['uid']) ? $data['uid']: '';
        $token=isset($data['token']) ? $data['token']: '';
        $p=isset($data['page']) ? $data['page']: '1';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        $p=checkNull($p);
		
		$result=array(
			'data'=>array(),
			'nums'=>0,
			'isscroll'=>0,
		);
	
		if(checkToken($uid,$token)==700){
			echo json_encode($result);
			exit;
		} 
		
		$pnums=50;
		$start=($p-1)*$pnums;
		
		$list_coin=Db::name('user_votesrecord')->where("uid={$uid} ")->order("addtime desc")->limit(0,50)->select()->toArray();		   
		foreach($list_coin as $k=>$v){
			$list_coin[$k]['addtime']=date('Y/m/d',$v['addtime']);
			$record_thumb="";
			$record_name="";
			$record_typename="";//消费类型：下单、退回、退款、送礼、活动（邀请奖励）
			$action =$v['action'];

			if($action=='1'){
				$record_typename="接单";
			}
			
				$orderinfo=Db::name("orders")->field("skillid")->where("id='{$v['actionid']}'")->find();
				if($orderinfo){
					$skillinfo=Db::name("skill")->field("thumb,name")->where("id='{$orderinfo['skillid']}'")->find();
					if(!$skillinfo){
						$skillinfo=array(
							"name"=>'技能已删除',
							"thumb"=>"/default.png"
						);
					}
					$record_thumb=get_upload_path($skillinfo['thumb']);
					$record_name=$skillinfo['name'];
				}
				
			
		
			$list_coin[$k]['record_thumb']=$record_thumb;
			$list_coin[$k]['record_name']=$record_name;
			$list_coin[$k]['record_typename']=$record_typename;
			
			$userinfo=getUserInfo($v['fromid']);
			if(!$userinfo){
				$userinfo=array(
					"user_nicename"=>'用户已删除'
				);
			}
			$list_coin[$k]['userinfo']=$userinfo;
		}

		$nums=count($list_coin);
		if($nums<$pnums){
			$isscroll=0;
		}else{
			$isscroll=1;
		}
		
		$result=array(
			'data'=>$list_coin,
			'nums'=>$nums,
			'isscroll'=>$isscroll,
		);

		echo json_encode($result);
		exit;
	}
	

}