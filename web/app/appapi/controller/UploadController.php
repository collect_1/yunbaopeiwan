<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————


/* 上传 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use cmf\lib\Upload;

class UploadController extends HomeBaseController
{
    /* 图片上传 */
	public function upload(){
        $file=isset($_FILES['file']) ? $_FILES['file']: [];
        if(!$file){
            echo json_encode(array( "code"=>0,'data'=>'','msg'=>lang('请选择图片') ));
            exit;
        }
        
        if($file['error']!=0){
            echo json_encode(array( "code"=>0,'data'=>'','msg'=>lang('请重新选择图片') ));
            exit;
        }
        //file_put_contents('./upload.txt',date('Y-m-d H:i:s').' 提交参数信息 file:'.json_encode($_FILES)."\r\n",FILE_APPEND);
        if ($this->request->isPost()) {
			//获取后台上传配置
			$configpri=getConfigPri();
			$cloudtype=$configpri['cloudtype'];
			
			if($cloudtype==1){//七牛云存储
				$uploader = new Upload();

				$result = $uploader->upload();

				if ($result === false) {
					//file_put_contents('./upload.txt',date('Y-m-d H:i:s').' 提交参数信息 msg:'.$uploader->getError()."\r\n",FILE_APPEND);
					//$this->error();
					//echo json_encode(array( "code"=>0,'data'=>'','msg'=>$uploader->getError() ));
					echo json_encode(array( "code"=>0,'data'=>'','msg'=>lang('上传失败，请重试') ));
					exit;
				} else {
					//$this->success("上传成功!", '', $result);
					//file_put_contents('./upload.txt',date('Y-m-d H:i:s').' 提交参数信息 result:'.json_encode($result)."\r\n",FILE_APPEND);
					echo json_encode(array("code"=>200,'data'=>$result,'msg'=>''));
					exit;
				}
			}else{ 
				//其他云存储
				$result=adminUploadFiles($_FILES['file'],$cloudtype);
				if ($result!==false) {
					$data=array( 
						'filepath' => $result,
						'name'=> '',
						'id' => 'WU_FILE_0', 
						'preview_url' => $configpri['aws_hosturl']."/".$result,
						'url' => $result,
						'code'		  =>0,
					);
					echo json_encode(array("code"=>200,'data'=>$data,'msg'=>''));
					exit;
				}else{
					echo json_encode(array( "code"=>0,'data'=>'','msg'=>lang('上传失败，请重试') ));
					exit;
				}
			}
        }
	}
}
