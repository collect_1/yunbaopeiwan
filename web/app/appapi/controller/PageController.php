<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

/* 内容管理 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use app\portal\service\PostService;
use think\Db;

class PageController extends HomebaseController{

	public function lists() {

		// $list=Db::name("portal_post")->field("id,post_title")->where("type='1'")->order("id desc")->select();

		// $this->assign("list",$list);
		 
		// return $this->fetch();
        
        $postService = new PostService();
        $pageId      = 4;
        $page        = $postService->publishedPage($pageId);

        if (empty($page)) {
            $this->assign('reason', lang('页面不存在'));
            return $this->fetch(':error');
        }

        $this->assign('uid', '');
        $this->assign('token', '');
        $this->assign('page', $page);


        return $this->fetch('detail');
	}	
    
    public function detail() {        
        $postService = new PostService();
        $pageId      = $this->request->param('id', 0, 'intval');
        $page        = $postService->publishedPage($pageId);

        if (empty($page)) {
            $this->assign('reason', lang('页面不存在'));
            return $this->fetch(':error');
        }
        
        $this->assign('uid', '');
        $this->assign('token', '');
        $this->assign('page', $page);
		
		
		$getConfigPub=getConfigPub();
		$site_url=$getConfigPub['site_url'].'/upload/';
        $this->assign('site_url', $site_url);
		
		//查询七牛地址
		$plugin= Db::name('plugin')
                    ->field('config')
                    ->where(['name'=>'Qiniu'])
                    ->find();
		$plugin_url='*********';
		if($plugin){
			$p_config=json_decode($plugin['config'],true);
			
			if($p_config['protocol']!='' || $p_config['domain']!=''){
				$plugin_url=$p_config['protocol'].'://'.$p_config['domain'];
		
			}

		}
					
        $this->assign('plugin_url', $plugin_url);


        return $this->fetch();
	}	
	

}