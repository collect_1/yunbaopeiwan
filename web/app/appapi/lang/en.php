<?php
//例： '你好，{n}'=>'hello,{n}'
return [
    '您的登陆状态失效，请重新登陆！'=> 'Your logging status is invalid, please login again!',
    '信息错误'=> 'Information error',

    /* Auth */    
    '成为认证大神'=> 'Become an authenticated master',
    '已阅读并同意'=> 'Read and accepted',
    '立即认证'=> 'Authenticate now',
    '大神认证协议'=> 'agreement',
    '请勾选大神认证协议'=> 'Please check the agreement',

    '实名认证'=> 'Real name authentication',
    '以下信息均为必填项，为保障您的利益，请如实填写'=> 'Please fill the  following information in truthfully',
    '真实姓名'=> 'Real name',
    '请输入姓名'=> 'Please type in name',
    '手机号码'=> 'Mobile',
    '请输入手机号码'=> 'Please type in your mobile phone number',
    '身份证号'=> 'ID No.',
    '请输入身份证号码'=> 'Please type in ID No.' ,
    '证件正面'=> 'The front of the certificate',
    '证件反面'=> 'The reverse side of the certificate',
    '手持证件正面照'=> 'The photo holding the front of the certificate',
    '信息审核中'=> 'Information is being checked',
    '提交认证'=> 'Submit authentication',

    '申请成功'=> 'application approved',
    '认证审核中，不能申请'=> 'You cannot conduct application for the authentication is being checked',
    '请上传证件正面'=> 'Please upload the front of the certificate',
    '请上传证件反面'=> 'Please upload the reverse side of the certificate',
    '请上传手持证件正面照'=> 'Please upload the the photo holding the front of the certificate',

    '身份信息审核未通过'=> 'Identity information check failed',
    '身份信息审核中...'=> 'identity information is being checked...',
    '3个工作日内会有审核结果，请耐心等待'=> 'There will be check results within 3 working days, please wait patiently',
    '重新认证'=> 'Re-authentication',
    
    /* cash */
    '提现记录'=> 'Discount Record',
    '金额'=> 'Amount',
    '审核中'=> 'In Review',
    '成功'=> 'Success',
    '失败'=> 'Fail',
    
    
    /* pay */
    '非法提交'=> 'Illegal submission',
    '订单信息不存在'=> 'Order information does not exist',
    '支付成功'=> 'Successful Payment ',
    '服务器出错，请联系管理员'=> 'Server error, please contact administrator',
    '验证失败，如有疑问请联系管理'=> 'Verification failed. if you have any questions, please contact administrator',
    '服务器出错，请联系管理员'=> 'Server error, please contact administrator',

    /* page */
    '页面不存在'=> 'Page does not exist',

    /* Skillauth */    
    '上传图片'=> 'Upload picture',
    '禁止盗用他人图片，发现将会封号'=> "It is forbidden to embezzle other people's pictures. If found, the account will be closed down",
    '段位'=> 'Dan grading',
    '必须与图片等级相符'=> 'Picture level matched',
    '提交审核'=> 'Submit for check',

    '请上传截图'=> 'Please upload screenshots',
    '请选择段位'=> 'Please select dan grading',
    '技能信息错误'=> 'Skill information error',
    '段位信息错误'=> 'dan grading information error',
    
    '大神技能审核未通过'=> 'Master skill check failed',
    '大神技能审核中...'=> 'Master skill is being checked...',
    
    /* page */
    '上传失败，请重试'=> 'Fail to upload, please try again',


];