<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————


/**
 * 订单收益记录
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class OrderrecordController extends AdminbaseController {
    
    protected function getTypes($k=''){
        $type=array(
            '0'=>'支出',
            '1'=>'收入',
        );
        if($k===''){
            return $type;
        }
        
        return isset($type[$k]) ? $type[$k]: '';
    }
    
    protected function getAction($k=''){
        $action=array(
            '1'=>'余额下单',
            '2'=>'订单退回',
            '4'=>'订单退款',
        );
        if($k===''){
            return $action;
        }
        
        return isset($action[$k]) ? $action[$k]: '未知';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        $map[]=['action','not in',('3,11')];//非礼物赠送/注册奖励
		
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $type=isset($data['type']) ? $data['type']: '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $action=isset($data['action']) ? $data['action']: '';
        if($action!=''){
            $map[]=['action','=',$action];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
           $map[]=['uid','=',$uid];
        }
        
        $touid=isset($data['touid']) ? $data['touid']: '';
        if($touid!=''){
            $map[]=['touid','=',$touid];
        }
		if($data['orderid']!=''){
			$map[]=['actionid','=',$data['orderid']];
        }
        
        
        $lists = Db::name("user_coinrecord")
            ->where($map)
			->order("id desc")
			->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['touserinfo']=getUserInfo($v['touid']);
            $orderinfo=Db::name("orders")->field("svctm,overtime")->where("id='{$v['actionid']}'")->find();
			if(!$orderinfo){
				$orderinfo['svctm']=0;
				$orderinfo['overtime']=0;
			}
			$v['orderinfo']= $orderinfo;
            return $v;           
        });
		$moneysum = Db::name("user_coinrecord")
            ->where($map)
			->sum('total');
        if(!$moneysum){
            $moneysum=0;
        }

		$this->assign('moneysum', $moneysum);
		
    	
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
        $this->assign('action', $this->getAction());
        $this->assign('type', $this->getTypes());
        
    	return $this->fetch();
    
    }
		
    function del(){
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('user_coinrecord')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
                    
        $this->success("删除成功！");
        							  			
    }   
	//导出：订单收支明细
	function export()
    {
        $data = $this->request->param();
        $map=[];
        $map[]=['action','not in',('3,11')];//非礼物赠送/注册奖励
		
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $type=isset($data['type']) ? $data['type']: '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $action=isset($data['action']) ? $data['action']: '';
        if($action!=''){
            $map[]=['action','=',$action];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
           $map[]=['uid','=',$uid];
        }
        
        $touid=isset($data['touid']) ? $data['touid']: '';
        if($touid!=''){
            $map[]=['touid','=',$touid];
        }
		if($data['orderid']!=''){
            $map[]=['actionid','=',$data['orderid']];
        }
        
        $xlsName  = "订单收支明细";

        $xlsData=Db::name("user_coinrecord")
            ->where($map)
            ->order('id desc')
			->select()
            ->toArray();
        foreach ($xlsData as $k => $v)
        {
            $userinfo=getUserInfo($v['uid']);
			$touserinfo=getUserInfo($v['touid']);
			
            $xlsData[$k]['user_nickname']= $userinfo['user_nickname']."(".$v['uid'].")";
            $xlsData[$k]['anchor_nickname']= $touserinfo['user_nickname']."(".$v['touid'].")";
			
			$orderinfo=Db::name("orders")->field("svctm,overtime")->where("id='{$v['actionid']}'")->find();
			if(!$orderinfo){
				$orderinfo['svctm']=0;
				$orderinfo['overtime']=0;
			}
            $xlsData[$k]['svctm']=date("Y-m-d H:i:s",$orderinfo['svctm']); 
            $xlsData[$k]['overtime']=date("Y-m-d H:i:s",$orderinfo['overtime']); 
			
            $xlsData[$k]['addtime']=date("Y-m-d H:i:s",$v['addtime']); 
			
            $xlsData[$k]['type']=$this->getTypes($v['type']);
            $xlsData[$k]['action']=$this->getAction($v['action']);
        }

       
        $configpub=getConfigPub();
        $cellName = array('A','B','C','D','E','F','G','H','I','J');
        $xlsCell  = array(
            array('id','序号'),
            array('type','收支类型'),
            array('action','收支行为'),
            array('user_nickname','下单方昵称 (ID)'),
            array('anchor_nickname','接单方昵称 (ID)'),
            array('svctm','服务时间'),
            array('overtime','结束时间'),
            array('nums','数量'),
            array('total','总价'),
            array('addtime','时间')
        );
        exportExcel($xlsName,$xlsCell,$xlsData,$cellName);
    }
 	
}
