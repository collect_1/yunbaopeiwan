<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\Menu;

class MainController extends AdminBaseController
{

    

    public function dashboardWidget(){
        $dashboardWidgets = [];
        $widgets          = $this->request->param('widgets/a');
        if (!empty($widgets)) {
            foreach ($widgets as $widget) {
                if ($widget['is_system']) {
                    array_push($dashboardWidgets, ['name' => $widget['name'], 'is_system' => 1]);
                } else {
                    array_push($dashboardWidgets, ['name' => $widget['name'], 'is_system' => 0]);
                }
            }
        }

        cmf_set_option('admin_dashboard_widgets', $dashboardWidgets, true);

        $this->success('更新成功!');

    }
	
	 public function index(){
        $nowtime=time();
        //当天0点
        $today=date("Ymd",$nowtime);
        $today_start=strtotime($today);
        //当天 23:59:59
        $today_end=strtotime("{$today} + 1 day");
        
        $yesterday_start=$today_start - 60*60*24;
        
        
        /* 充值统计 */
        /* 支付金额 */
        $pay_money=Db::name('charge_user')->where("status=1")->sum('money');
        if(!$pay_money){
            $pay_money=0;
        }
        
        $pay_money_t=Db::name('charge_user')->where("status=1 and addtime >= {$today_start} and addtime < {$today_end}")->sum('money');
        if(!$pay_money_t){
            $pay_money_t=0;
        }
        
        $pay_money_y=Db::name('charge_user')->where("status=1 and addtime >= {$yesterday_start} and addtime < {$today_start}")->sum('money');
        if(!$pay_money_y){
            $pay_money_y=0;
        }
        
        $pay_money_rate='0%';
        $pay_money_rate_c='';
        if($pay_money_t==0 && $pay_money_y==0){
            
        }else if($pay_money_t==0){
            $pay_money_rate=$pay_money_y.'%';
            $pay_money_rate_c='down';
        }else if($pay_money_y==0){
            $pay_money_rate=$pay_money_t.'%';
            $pay_money_rate_c='up';
        }else{
            $rate=floor(($pay_money_t - $pay_money_y)/$pay_money_y*100);
            if($rate>0){
                $pay_money_rate_c='up';
            }else if($rate<0){
                $pay_money_rate_c='down';
            }
            $pay_money_rate=abs($rate).'%';
        }
        
        $this->assign('pay_money', $pay_money);
        $this->assign('pay_money_t', $pay_money_t);
        $this->assign('pay_money_y', $pay_money_y);
        $this->assign('pay_money_rate', $pay_money_rate);
        $this->assign('pay_money_rate_c', $pay_money_rate_c);
        
        /* 支付订单 */
        $pay_orders=Db::name('charge_user')->where("status=1")->count();
        if(!$pay_orders){
            $pay_orders=0;
        }
        
        $pay_orders_t=Db::name('charge_user')->where("status=1 and addtime >= {$today_start} and addtime < {$today_end}")->count();
        if(!$pay_orders_t){
            $pay_orders_t=0;
        }
        
        $pay_orders_y=Db::name('charge_user')->where("status=1 and addtime >= {$yesterday_start} and addtime < {$today_start}")->count();
        if(!$pay_orders_y){
            $pay_orders_y=0;
        }
        
        $pay_orders_rate='0%';
        $pay_orders_rate_c='';
        if($pay_orders_t==0 && $pay_orders_y==0){
            
        }else if($pay_orders_t==0){
            $pay_orders_rate=$pay_orders_y.'%';
            $pay_orders_rate_c='down';
        }else if($pay_orders_y==0){
            $pay_orders_rate=$pay_orders_t.'%';
            $pay_orders_rate_c='up';
        }else{
            $rate=floor(($pay_orders_t - $pay_orders_y)/$pay_orders_y*100);
            if($rate>0){
                $pay_orders_rate_c='up';
            }else if($rate<0){
                $pay_orders_rate_c='down';
            }
            $pay_orders_rate=abs($rate).'%';
        }
        
        $this->assign('pay_orders', $pay_orders);
        $this->assign('pay_orders_t', $pay_orders_t);
        $this->assign('pay_orders_y', $pay_orders_y);
        $this->assign('pay_orders_rate', $pay_orders_rate);
        $this->assign('pay_orders_rate_c', $pay_orders_rate_c);
        
        /* 支付人数 */
        $pay_users=Db::name('charge_user')->where("status=1")->group('uid')->count();
        if(!$pay_users){
            $pay_users=0;
        }
        
        $pay_users_t=Db::name('charge_user')->where("status=1 and addtime >= {$today_start} and addtime < {$today_end}")->group('uid')->count();
        if(!$pay_users_t){
            $pay_users_t=0;
        }
        
        $pay_users_y=Db::name('charge_user')->where("status=1 and addtime >= {$yesterday_start} and addtime < {$today_start}")->group('uid')->count();
        if(!$pay_users_y){
            $pay_users_y=0;
        }
        
        $pay_users_rate='0%';
        $pay_users_rate_c='';
        if($pay_users_t==0 && $pay_users_y==0){
            
        }else if($pay_users_t==0){
            $pay_users_rate=$pay_users_y.'%';
            $pay_users_rate_c='down';
        }else if($pay_users_y==0){
            $pay_users_rate=$pay_users_t.'%';
            $pay_users_rate_c='up';
        }else{
            $rate=floor(($pay_users_t - $pay_users_y)/$pay_users_y*100);
            if($rate>0){
                $pay_users_rate_c='up';
            }else if($rate<0){
                $pay_users_rate_c='down';
            }
            $pay_users_rate=abs($rate).'%';
        }
        
        $this->assign('pay_users', $pay_users);
        $this->assign('pay_users_t', $pay_users_t);
        $this->assign('pay_users_y', $pay_users_y);
        $this->assign('pay_users_rate', $pay_users_rate);
        $this->assign('pay_users_rate_c', $pay_users_rate_c);
        
        /* 注册统计 */
        /* 注册人数 */
        $users=Db::name('user')->where("user_type=2")->count();
        if(!$users){
            $users=0;
        }
        
        $users_t=Db::name('user')->where("user_type=2 and create_time >= {$today_start} and create_time < {$today_end}")->count();
        if(!$users_t){
            $users_t=0;
        }
        
        $users_y=Db::name('user')->where("user_type=2 and create_time >= {$yesterday_start} and create_time < {$today_start}")->count();
        if(!$users_y){
            $users_y=0;
        }
        
        $users_rate='0%';
        $users_rate_c='';
        if($users_t==0 && $users_y==0){
            
        }else if($users_t==0){
            $users_rate=$users_y.'%';
            $users_rate_c='down';
        }else if($users_y==0){
            $users_rate=$users_t.'%';
            $users_rate_c='up';
        }else{
            $rate=floor(($users_t - $users_y)/$users_y*100);
            if($rate>0){
                $users_rate_c='up';
            }else if($rate<0){
                $users_rate_c='down';
            }
            $users_rate=abs($rate).'%';
        }
        
        $this->assign('users', $users);
        $this->assign('users_t', $users_t);
        $this->assign('users_y', $users_y);
        $this->assign('users_rate', $users_rate);
        $this->assign('users_rate_c', $users_rate_c);
        
        
        
        
        /* 提现 */
        $cash=Db::name('cash_record')->where("status=1 ")->sum('money');
        if(!$cash){
            $cash=0;
        }
        
        $cash_t=Db::name('cash_record')->where("status=1 and uptime >= {$today_start} and uptime < {$today_end}")->sum('money');
        if(!$cash_t){
            $cash_t=0;
        }
        
        $cash_y=Db::name('cash_record')->where("status=1 and uptime >= {$yesterday_start} and uptime < {$today_start}")->sum('money');
        if(!$cash_y){
            $cash_y=0;
        }
        
        $cash_rate='0%';
        $cash_rate_c='';
        if($cash_t==0 && $cash_y==0){
            
        }else if($cash_t==0){
            $cash_rate=$cash_y.'%';
            $cash_rate_c='down';
        }else if($cash_y==0){
            $cash_rate=$cash_t.'%';
            $cash_rate_c='up';
        }else{
            $rate=floor(($cash_t - $cash_y)/$cash_y*100);
            if($rate>0){
                $cash_rate_c='up';
            }else if($rate<0){
                $cash_rate_c='down';
            }
            $cash_rate=abs($rate).'%';
        }
        $this->assign('cash', $cash);
        $this->assign('cash_t', $cash_t);
        $this->assign('cash_y', $cash_y);
        $this->assign('cash_rate', $cash_rate);
        $this->assign('cash_rate_c', $cash_rate_c);
        
        
        
		
		
		
		/* 技能 */
        $skills=Db::name('skill_auth')->where("status=0")->count();
        if(!$skills){
            $skills=0;
        }
        
        $skills_t=Db::name('skill_auth')->where("status=0 and addtime >= {$today_start} and addtime < {$today_end}")->count();
        if(!$skills_t){
            $skills_t=0;
        }
        
        $skills_y=Db::name('skill_auth')->where("status=0 and addtime >= {$yesterday_start} and addtime < {$today_start}")->count();
        if(!$skills_y){
            $skills_y=0;
        }
        
        $skills_rate='0%';
        $skills_rate_c='';
        if($skills_t==0 && $skills_y==0){
            
        }else if($skills_t==0){
            $skills_rate=$skills_y.'%';
            $skills_rate_c='down';
        }else if($skills_y==0){
            $skills_rate=$skills_t.'%';
            $skills_rate_c='up';
        }else{
            $rate=floor(($skills_t - $skills_y)/$skills_y*100);
            if($rate>0){
                $skills_rate_c='up';
            }else if($rate<0){
                $skills_rate_c='down';
            }
            $skills_rate=abs($rate).'%';
        }
        $this->assign('skills', $skills);
        $this->assign('skills_t', $skills_t);
        $this->assign('skills_y', $skills_y);
        $this->assign('skills_rate', $skills_rate);
        $this->assign('skills_rate_c', $skills_rate_c);
		
		
        return $this->fetch();
    }
	
	
	
	


	
	
	
}
