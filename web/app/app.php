<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————


return [

    // 是否开启多语言
    'lang_switch_on'         => true,
    // 默认语言
    'default_lang'           => 'zh-cn',
	// 默认全局过滤方法 用逗号分隔多个
	// 'default_filter'         => 'htmlspecialchars,stripslashes,htmlentities,strip_tags',
	'default_filter'         => 'htmlspecialchars,stripslashes,strip_tags',
];


