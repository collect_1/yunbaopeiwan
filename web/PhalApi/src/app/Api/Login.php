<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
namespace App\Api;

use PhalApi\Api;
use App\Domain\Login as Domain_Login;

/**
 * 注册、登录
 */
if (!session_id()) session_start();

class Login extends Api {
    public function getRules() {
        return array(
            'login' => array(
                'username' => array('name' => 'username', 'type' => 'string', 'desc' => '用户名'),
                'code' => array('name' => 'code', 'type' => 'string', 'desc' => '验证码'),
				'country_code' => array('name' => 'country_code', 'type' => 'string', 'desc' => '账号-区号'),
                'source' => array('name' => 'source', 'type' => 'int',  'default'=>'0', 'desc' => '来源设备,0web，1android，2ios，3小程序'),
            ),
            
            'getCode' => array(
				'account' => array('name' => 'account', 'type' => 'string', 'desc' => '手机号码'),
				'country_code' => array('name' => 'country_code', 'type' => 'string', 'desc' => '区号'),																			  
                'sign' => array('name' => 'sign', 'type' => 'string',  'default'=>'', 'desc' => '签名'),
			),
			
			'getCountrys'=>array(
            	'field' => array('name' => 'field', 'type' => 'string', 'default'=>'', 'desc' => '搜索json串'),
            ), 
        );
    }
    
    
    /**
     * 登录方式开关信息
     * @desc 用于获取登录方式开关信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[][0] 登录方式标识
     * @return string msg 提示信息
     */
     
    public function getLoginType() {
		
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

        $info = \App\getConfigPub();
		
		$configpri=\App\getConfigPri();
      

        //登录弹框那个地方
        $login_alert=array(
            'title'=>$info['login_alert_title'],
            'content'=>$info['login_alert_content'],
            'login_title'=>$info['login_clause_title'],
            'message'=>array(
                array(
                    'title'=>$info['login_service_title'],
                    'url'=>\App\get_upload_path($info['login_service_url']),
                ),
                array(
                    'title'=>$info['login_private_title'],
                    'url'=>\App\get_upload_path($info['login_private_url']),
                ),
            )
        );
		
		$login_type=$configpri['login_type'];
        foreach ($login_type as $k => $v) {
            if($v=='ios'){
                unset($login_type[$k]);
                break;
            }
        }

        $login_type=array_values($login_type);

        $rs['info'][0]['login_alert'] = $login_alert;
        $rs['info'][0]['login_type'] = $login_type;
		$rs['info'][0]['login_type_ios'] = $configpri['login_type'];

        return $rs;
    }
    
    /**
     * 登陆
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nickname 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].level_anchor 主播等级
     * @return string info[0].birthday 生日
     * @return string info[0].age 年龄
     * @return string info[0].token 用户Token
     * @return string info[0].isauth 是否认证，0否1是
     * @return string info[0].usersig IM签名
     * @return string msg 提示信息
     */
    public function login() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $username = \App\checkNull($this->username);
        $code = \App\checkNull($this->code);
        $country_code = \App\checkNull($this->country_code);
        $source = \App\checkNull($this->source);
        
        if($username==''){
            $rs['code'] = 995;
            $rs['msg'] = \PhalApi\T('请输入手机号');
            return $rs;
        }
		if($country_code!=$_SESSION['reg_country_code']){
            $rs['code'] = 1004;
            $rs['msg'] = '国家区号不一致';
            return $rs;					
		}
        
        if($code==''){
            $rs['code'] = 996;
            $rs['msg'] = \PhalApi\T('请输入验证码');
            return $rs;
        }
        
        
        if(!isset($_SESSION['reg_account']) || !$_SESSION['reg_account'] || !isset($_SESSION['reg_code']) || !$_SESSION['reg_code'] ){
            $rs['code'] = 996;
            $rs['msg'] = \PhalApi\T('请先获取验证码');
            return $rs;		
        }

        if(time() - $_SESSION['reg_expiretime'] > 5*60){
            $rs['code'] = 996;
            $rs['msg'] = \PhalApi\T('验证码已过期，请重新获取');
            return $rs;		
        }
        
        if($_SESSION['reg_account']!=$username){
            $rs['code'] = 995;
            $rs['msg'] = \PhalApi\T('手机号码错误');
            return $rs;		
        }
        
        if($_SESSION['reg_code']!=$code){
            $rs['code'] = 996;
            $rs['msg'] = \PhalApi\T('验证码错误');
            return $rs;		
        }
        
        $domain = new Domain_Login();
        $info = $domain->userLogin($username,$source);

        return $info;
    }
    
	/**
	 * 获取注册验证码
	 * @desc 用于注册获取验证码
	 * @return int code 操作码，0表示成功,2发送失败
	 * @return array info 
	 * @return string msg 提示信息
	 */
	public function getCode() {
		$rs = array('code' => 0, 'msg' => \PhalApi\T('发送成功，请注意查收'), 'info' => array());
		
		$account = \App\checkNull($this->account);
		$country_code = \App\checkNull($this->country_code);											  
		$sign = \App\checkNull($this->sign);
		
        if($account==''){
			$rs['code']=995;
			$rs['msg']=\PhalApi\T('请输入手机号');
			return $rs;
		}
        
		$isok=\App\checkMobile($account,$country_code);
		if($isok==1001){
			$rs['code']=1001;
			$rs['msg']=\PhalApi\T('国际/港澳台不支持国内区号');
			return $rs;
		}else if($isok==1002){
			$rs['code']=1002;
			$rs['msg']='国内不支持国际/港澳台区号';
			return $rs;	
		}
		
		if(!$isok){
			$rs['code']=995;
			$rs['msg']=\PhalApi\T('请输入正确的手机号');
			return $rs;
		}
        
        $checkdata=array(
            'account'=>$account
        );
        	
		if(isset($_SESSION['reg_account']) && $_SESSION['reg_account']==$account && isset($_SESSION['reg_expiretime']) && $_SESSION['reg_expiretime']> time() ){
			$rs['code']=996;
			$rs['msg']=\PhalApi\T('验证码5分钟有效，请勿多次发送');
			return $rs;
		}

		$code = \App\random(6);
		
		/* 发送验证码 */
 		$result=\App\sendCode($account,$code,$country_code);
		if($result['code']==0){
			$_SESSION['reg_account'] = $account;
			$_SESSION['reg_country_code'] = $country_code;
			$_SESSION['reg_code'] = $code;
			$_SESSION['reg_expiretime'] = time() +60*5;	
		}else if($result['code']==667){
			$_SESSION['reg_account'] = $account;
			$_SESSION['reg_country_code'] = $country_code;
            $_SESSION['reg_code'] = $result['msg'];
            $_SESSION['reg_expiretime'] = time() +60*5;
            
            $rs['code']=1002;
			$rs['msg']=\PhalApi\T('验证码为：{n}',[ 'n'=>$result['msg'] ]);
		}else{
			$rs['code']=1002;
			$rs['msg']=$result['msg'];
		} 

		return $rs;
	}
    
    
	/*
	 * 获取国家列表
	 * @desc 用于获取国家列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].id 视频记录ID
	 * @return string msg 提示信息
	 */
	public function getCountrys() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$field=\App\checkNull($this->field);
		
		$key='getCountrys';
		$info=\App\getcaches($key);
		if(!$info){
			$city=API_ROOT.'/../PhalApi/config/country.json';//文件存放目录
			// 从文件中读取数据到PHP变量 
			$json_string = file_get_contents($city); 
			 // 用参数true把JSON字符串强制转成PHP数组 
			$data = json_decode($json_string, true);

			$info=$data['country']; //国家
			
			\App\setcaches($key,$info);
		}
		if($field){
			$rs['info']=$this->array_searchs($field,$info);
			return $rs;
		}
	 
		$rs['info']=$info;
		return $rs;
	}
	function array_searchs($field,$data) {

		$arr=$result=array();
		foreach($data as $k => $v){
		
			$lists=$v['lists'];
		
			foreach ($lists as $key => $value) {
				
				if(strstr($value['name'], $field) !== false){
					
					array_push($arr, $value);
				}
				
			}
			foreach ($arr as $key => $value) {
				if(array_key_exists($value,$lists)){
					array_push($result, $lists[$value]);
				}
			}
		
		}
		return $arr;
	}	 
} 
