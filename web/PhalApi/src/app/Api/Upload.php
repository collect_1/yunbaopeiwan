<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————


namespace App\Api;

use PhalApi\Api;

/**
 * 上传
 */
 
class Upload extends Api {

	public function getRules() {
		return array(
		);
	}
    
    /**
	 * 获取七牛Token
	 * @desc 用于获取充值规则
	 * @return int code 操作码，0表示成功，
	 * @return array  info 
	 * @return string info[0].token 七牛Token
	 * @return string msg 提示信息
	 */
	public function getQiniuToken() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=\App\checkNull($this->uid);
        $token=\App\checkNull($this->token);	
        
		$checkToken=\App\checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = \PhalApi\T('您的登陆状态失效，请重新登陆！');
			return $rs;
		}

		$token = \PhalApi\DI()->qiniu->getToken();
        
        $token2=\App\encryption($token);
        $info['token']=$token2;
        $rs['info'][0] =$info;
        
		return $rs;
	}
	
	public function getQiniuTokenstr() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
        
		$token = \PhalApi\DI()->qiniu->getToken();
        
        $token2=\App\encryption($token);
        $info['token']=$token2;
        $rs['info'][0] =$info;
        
		return $token2;
	}
	
	/**
     * 获取云存储方式、获取七牛上传验证token字符串、获取腾讯云存储相关配置信息
     * @desc 用于获取云存储方式、获取七牛上传验证token字符串、获取腾讯云存储相关配置信息
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     */

    public function getCosInfo(){

        $rs=array("code"=>0,"msg"=>"","info"=>array());

		/* $uid=\App\checkNull($this->uid);
        $token=\App\checkNull($this->token);	
        
		$checkToken=\App\checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = \PhalApi\T('您的登陆状态失效，请重新登陆！');
			return $rs;
		} 
		 */
        //获取七牛信息
        $qiniuToken=$this->getQiniuTokenstr();

        //获取腾讯云存储配置信息
		$configpri=\App\getConfigPri();

        if(!$configpri['cloudtype']){
            $rs['code']=1001;
            $rs['msg']="无指定存储方式";
            return $rs;
        }
		$space_host= \PhalApi\DI()->config->get('app.Qiniu.space_host');
		$region= \PhalApi\DI()->config->get('app.Qiniu.region');
		
		$qiniu_zone='';
		// $zone=$configpri['zone'];
		if($region=='z0'){
			$qiniu_zone='qiniu_hd';
		}else if($region=='z1'){
			$qiniu_zone='qiniu_hb';
		}else if($region=='z2'){
			$qiniu_zone='qiniu_hn';
		}else if($region=='na0'){
			$qiniu_zone='qiniu_bm';
		}else if($region=='as0'){
			$qiniu_zone='qiniu_xjp';
		}
		
		// $qiniu_domain_url=$configpri['protocol']."://".$configpri['domain']."/";
		
        $qiniuInfo=array(
            'qiniuToken'=>$qiniuToken,
            'qiniu_domain'=>$space_host,
            'qiniu_zone'=>$qiniu_zone  //华东:qiniu_hd 华北:qiniu_hb  华南:qiniu_hn  北美:qiniu_bm   新加坡:qiniu_xjp 不可随意更改，app已固定好规则
        );

        $awsInfo=array(
            'aws_bucket'=>$configpri['aws_bucket'],
            'aws_region'=>$configpri['aws_region'],
            'aws_identitypoolid'=>$configpri['aws_identitypoolid'],
        );
        
        $rs['info'][0]['qiniuInfo']=$qiniuInfo;
        $rs['info'][0]['awsInfo']=$awsInfo;

        $cloudtype="";
        switch ($configpri['cloudtype']) {
            case '1':
                $cloudtype="qiniu";
                break;

            case '2':
                $cloudtype="aws";
                break;
         } 

        $rs['info'][0]['cloudtype']=$cloudtype;
        
        return $rs;
        
    }
	
    
}
