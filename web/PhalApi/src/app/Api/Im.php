<?php
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————


namespace App\Api;

use PhalApi\Api;
use App\Domain\Im as Domain_Im;

/**
 * 私信
 */
 
class Im extends Api {

	public function getRules() {
		return array(
            
			'getSysNotice' => array(
				'p' => array('name' => 'p', 'type' => 'string', 'desc' => '页码'),
			),
			
		);
	}
    
	/**
	 * 系统通知 
	 * @desc 用于获取系统通知
	 * @return int code 操作码，0表示成功
	 * @return array info 列表
	 * @return string info[].content 信息内容
	 * @return string info[].addtime 时间
	 * @return string msg 提示信息
	 */
	public function getSysNotice() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
        $uid=\App\checkNull($this->uid);
        $token=\App\checkNull($this->token);
        $p=\App\checkNull($this->p);
        
        $checkToken=\App\checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = \PhalApi\T('您的登陆状态失效，请重新登陆！');
			return $rs;
		}
        
        $domain = new Domain_Im();
		$list = $domain->getSysNotice($uid,$p);

        $rs['info']=$list;
		return $rs;
	}   
	
}
