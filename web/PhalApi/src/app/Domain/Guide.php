<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

namespace App\Domain;

use App\Model\Guide as Model_Guide;

class Guide {
	public function getGuide() {
		
		$rs = array();

		$model = new Model_Guide();
		$rs = $model->getGuide();

		return $rs;
	}
}
