<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

namespace App\Domain;

use App\Model\Im as Model_Im;

class Im {

    /* 系统通知 */
	public function getSysNotice($uid,$p) {

        $model = new Model_Im();
        $list= $model->getSysNotice($uid,$p);

        foreach($list as $k=>$v){
            unset($v['ip']);
            $v['addtime']=date("Y-m-d H:i:s",$v['addtime']);
            
            $list[$k]=$v;
        }
		return $list;
	}
	
	
}
