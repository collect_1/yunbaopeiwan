<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

namespace App\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Login extends NotORM {

	protected $fields='id,user_nickname,avatar,avatar_thumb,sex,signature,coin,votes,birthday,user_status,login_type,last_login_time,profession,school,hobby,voice,voice_l,addr,stars,star_nums,orders,consumption,votestotal,votes_gifttotal';

	/* 会员登录 */   	
    public function userLogin($where) {

		$info=\PhalApi\DI()->notorm->user
				->select($this->fields)
				->where('user_type=2')
                ->where($where)
				->fetchOne();
                
        return $info;
    }	
	
	/* 会员注册 */
    public function userReg($data=[]) {
        
        $nowtime=time();
        $user_pass='yuewan'.$nowtime;
        $user_pass=\App\setPass($user_pass);

        $avatar='/default.png';
        $avatar_thumb='/default_thumb.png';
		$configpri=\App\getConfigPri();
		$reg_reward=$configpri['reg_reward'];
		
        $default=array(
            'user_pass' =>$user_pass,
            'signature' =>'',
            'avatar' =>$avatar,
            'avatar_thumb' =>$avatar_thumb,
            'last_login_ip' =>\PhalApi\Tool::getClientIp(),
            'create_time' => $nowtime,
            'user_status' => 1,
            "user_type"=>2,//会员
			"coin"=>$reg_reward,
        );
        
        if(isset($data['user_pass'])){
            $data['user_pass']=\App\setPass($data['user_pass']);
        }
        $insert=array_merge($default,$data);
        
            
		$rs=\PhalApi\DI()->notorm->user->insert($insert);
        
        $id=$rs['id'];
		if($reg_reward>0){
			$insert=array("type"=>'1',"action"=>'11',"uid"=>$id,"touid"=>$id,"actionid"=>0,"nums"=>1,"total"=>$reg_reward,"addtime"=>time() );
			\PhalApi\DI()->notorm->user_coinrecord->insert($insert);
		}
        
        $info=\PhalApi\DI()->notorm->user
				->select($this->fields)
				->where('id=?',$id)
				->fetchOne();
                
		return $info;
    }

	
	/* 更新token 登陆信息 */
    public function updateToken($uid,$token,$data=array()) {
        
        $nowtime=time();
		$expiretime=$nowtime+60*60*24*150;

		\PhalApi\DI()->notorm->user
			->where('id=?',$uid)
			->update(array('last_login_time' => $nowtime, "last_login_ip"=>\PhalApi\Tool::getClientIp() ));

		$token_info=array(
			'user_id'=>$uid,
			'token'=>$token,
			'expire_time'=>$expiretime,
			'create_time'=>$nowtime,
		);
        $isexist=\PhalApi\DI()->notorm->user_token
			->where('user_id=?',$uid)
			->update( $token_info );
        if(!$isexist){
            \PhalApi\DI()->notorm->user_token
                ->insert( $token_info );
        }
		
		\App\hMSet("token_".$uid,$token_info);		
        
		return 1;
    }

    
	public function getcashRecord(){
		$recordlist=\PhalApi\DI()->notorm->cash_record
					->where("uid=? and status=0",$uid)
					->fetchAll();
		return $recordlist;
	}
	
	public function getOrderlist($uid){
		$list=\PhalApi\DI()->notorm->orders
					->where("(uid=? or liveuid=?) and status in(0,2,3,4,6)",$uid,$uid)
					->select("id,uid,status")
					->fetchAll(); //0待支付；2：已接单；3:等待退款；4：拒绝退款；6：退款申诉
		return $list;
	}
	
}
