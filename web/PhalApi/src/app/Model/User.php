<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

namespace App\Model;

use PhalApi\Model\NotORMModel as NotORM;

class User extends NotORM {

	/* 用户全部信息 */
	public function getBaseInfo($uid) {
		$info=\PhalApi\DI()->notorm->user
				->select("id,user_nickname,avatar,avatar_thumb,sex,signature,coin,votes,consumption,votestotal,votes_gift,votes_gifttotal,birthday,profession,school,hobby,voice,voice_l,addr,stars,star_nums")
				->where('id=?  and user_type=2',$uid)
				->fetchOne();
		
		return $info;
	}
    
    /* 是否认证 */
    public function isauth($uid){
        $isexist=\PhalApi\DI()->notorm->user_auth
				->select("uid")
				->where('uid =?  and status=1',$uid)
				->fetchOne();
        if($isexist){
            return '1';
        }
        
        return '0';
    }
    
    /* 检测用户昵称 */
    public function checkNickname($uid,$name){
        $isexist=\PhalApi\DI()->notorm->user
				->select("id")
				->where('id!=?  and user_nickname=?',$uid,$name)
				->fetchOne();
        if($isexist){
            return 1;
        }
        
        return 0;
    }

    /* 检测性别 */
    public function checkSex($uid){
        $isexist=\PhalApi\DI()->notorm->user
				->select("sex")
				->where('id!=?',$uid)
				->fetchOne();
        if($isexist && $isexist['sex']!=0){
            return 1;
        }
        
        return 0;
    }
    
    /* 用户信息更新 */
	public function upUserInfo($uid,$data){
        $rs=0;
        if($data){
            $rs=\PhalApi\DI()->notorm->user
                ->where('id=? ',$uid)
                ->update($data);
        }
        
        return $rs;
	}
    
    /* 更新星级 */
    public function upStar($uid,$stars,$nums) {

		$rs=\PhalApi\DI()->notorm->user
                ->where('id=?',$uid)
				->update(['stars' => new \NotORM_Literal("stars + {$stars}"),'star_nums' => new \NotORM_Literal("star_nums + {$nums}")]);
        
        $key='userinfo_'.$uid;
        
        $info=\App\hGetAll("userinfo_".$uid);
        if($info){
            \App\hIncrByFloat($key,'stars',$stars);
            \App\hIncrByFloat($key,'star_nums',$nums);
        }
        
            
        return $rs;
	}
    
    /* 设置关注 */
    public function setAttent($uid,$touid){
        
        $data=[
            'uid'=>$uid,
            'touid'=>$touid,
            'addtime'=>time(),
        ];
        
        $list=\PhalApi\DI()->notorm->user_attention
				->insert($data);
        
        return $list;
    }

    /* 取消关注 */
    public function delAttent($uid,$touid){
        
        $where=[
            'uid'=>$uid,
            'touid'=>$touid,
        ];
        
        $list=\PhalApi\DI()->notorm->user_attention
                ->where($where)
				->delete();
        
        return $list;
    }

    /* 关注列表 */
    public function getAttention($where,$p){
        
        if($p<1){
            $p=1;
        }
        
        $nums=50;
        $start=($p-1) * $nums;
        
        $list=\PhalApi\DI()->notorm->user_attention
				->select('*')
                ->where($where)
				->order('addtime desc')
                ->limit($start,$nums)
				->fetchAll();
        
        return $list;
    }
    
    /* 所有关注、粉丝用户 */
    public function getAllAttention($where,$order=''){
        
        $list=\PhalApi\DI()->notorm->user_attention
				->select('*')
                ->where($where)
				->order($order)
				->fetchAll();
        
        return $list;
    }
    
    /* 根据条件获取用户ID */
    public function getUsers($where){
        
        $list=\PhalApi\DI()->notorm->user
				->select('id')
                ->where($where)
				->fetchAll();
        
        return $list;
    }
    
    /* 是否派单主持人 */
    public function ishost($uid){
        $isexist=\PhalApi\DI()->notorm->user
				->select("id")
				->where('id =?  and ishost=1',$uid)
				->fetchOne();
        if($isexist){
            return '1';
        }
        
        return '0';
    }
	
	/* 用户列表：type：0：推荐；1：关注；2：最新 */
    public function getList($where,$order,$p,$type='-1'){
        
        if($p<1){
            $p=1;
        }
        if($type=='2'){
			$nums=3;
		}else{
			$nums=50;
		}
		
        $start=($p-1) * $nums;
		if($type=='0'){
			$configpri=\App\getConfigPri();
			$skill_recom_star=floatval($configpri['skill_recom_star']);
			$skill_recom_orders=floatval($configpri['skill_recom_orders']);
			$where .=" and isauth=1";
			
			$select_str="id,recommend_time,isauth,floor( (  orders * {$skill_recom_orders} ) * 100 ) as recom";
		
			$order="recommend_time desc,recom desc,id desc";
		}else{
			$select_str="id";
		}
	
     
        $list=\PhalApi\DI()->notorm->user
				->select($select_str)
                ->where($where)
				->order($order)
                ->limit($start,$nums)
				->fetchAll();
		
	
        foreach($list as $k=>$v){
			unset($list[$k]['recom']);
			$userinfo=\App\getUserInfo($v['id']);
		
			if($userinfo){
				unset($userinfo['birthday']);
				$isattent='0';
				if($uid==$touid){
					$isattent='1';
				}else{
					$isattent=\App\isAttent($uid,$v['touid']);
				}
				$userinfo['isattent']=$isattent;
				$list[$k]=$userinfo;
			}else{
				unset($list[$k]);
				continue;
			}
			
		}
		
		$list=array_values($list);
        return $list;
    }
	
	/* 最新认证三天的用户 */
    public function getAuthlist($where,$order){
       
        $list=\PhalApi\DI()->notorm->user_auth
				->select('*')
                ->where($where)
				->order($order)
				->fetchAll();
        return $list;
    }
	
}
