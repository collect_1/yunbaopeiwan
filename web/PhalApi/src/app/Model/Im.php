<?php

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

namespace App\Model;

use PhalApi\Model\NotORMModel as NotORM;

class Im extends NotORM {
    
	/* 官方公告 */
	public function getSysNotice($uid,$p) {
		
        
		if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$uid;
		//读取用户创建时间
		$userinfo=\PhalApi\DI()->notorm->user
				->select("create_time")
				->where('id =? ',$uid)
				->fetchOne();
		$list=\PhalApi\DI()->notorm->sys_notice
					->select("*")
					->where("addtime > ?",$userinfo['create_time'])								
					->limit($start,$pnum)
                    ->order('id desc')
					->fetchAll();
		//记录最后一次访问该接口的时间
		if($list){
			$isexist=\PhalApi\DI()->notorm->sys_notice_readtime
				->where('uid =? ',$uid)
				->fetchOne();
			if($isexist){
				
				\PhalApi\DI()->notorm->sys_notice_readtime
						->where("uid=?",$uid)
						->update(array("addtime"=>time()));
			}else{
				
				$data=array(
					"uid"=>$uid,
					"addtime"=>time()
				);
				\PhalApi\DI()->notorm->sys_notice_readtime
							->insert($data);
			}
		}
		
		return $list;

	}	


}
