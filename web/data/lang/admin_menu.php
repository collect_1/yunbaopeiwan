<?php
return array (
  'ADMIN_ADMIN_EDITPOST' => '编辑提交',
  'ADMIN_AGENT_DEFAULT' => '邀请赚钱',
  'ADMIN_AGENT_INDEX' => '邀请关系',
  'ADMIN_AGENT_PROFIT' => '邀请收益',
  'ADMIN_AUTH_INDEX' => '实名认证',
  'ADMIN_AUTH_SETSTATUS' => '审核',
  'ADMIN_CASH_INDEX' => '提现',
  'ADMIN_CASH_SETCASH' => '审核',
  'ADMIN_CHARGE_DEFAULT' => '财务管理',
  'ADMIN_CHARGE_INDEX' => '充值记录',
  'ADMIN_CHARGE_SETPAY' => '手动确认',
  'ADMIN_CHARGERULE_ADD' => '添加',
  'ADMIN_CHARGERULE_ADDPOST' => '添加提交',
  'ADMIN_CHARGERULE_DEL' => '删除',
  'ADMIN_CHARGERULE_EDIT' => '编辑',
  'ADMIN_CHARGERULE_EDITPOST' => '编辑提交',
  'ADMIN_CHARGERULE_INDEX' => '充值规则',
  'ADMIN_CHARGERULE_LISTORDER' => '排序',
  'ADMIN_DRIP_DEFAULT' => '滴滴订单',
  'ADMIN_DRIPCANCEL_ADD' => '添加',
  'ADMIN_DRIPCANCEL_ADDPOST' => '添加提交',
  'ADMIN_DRIPCANCEL_DEL' => '删除',
  'ADMIN_DRIPCANCEL_EDIT' => '编辑',
  'ADMIN_DRIPCANCEL_EDITPOST' => '编辑提交',
  'ADMIN_DRIPCANCEL_INDEX' => '取消原因',
  'ADMIN_DRIPCANCEL_LISTORDER' => '排序',
  'ADMIN_DYNAMIC_DEFAULT' => '动态管理',
  'ADMIN_DYNAMIC_DEL' => '删除',
  'ADMIN_DYNAMIC_INDEX' => '审核通过列表',
  'ADMIN_DYNAMIC_NOPASS' => '未通过列表',
  'ADMIN_DYNAMIC_SEE' => '查看',
  'ADMIN_DYNAMIC_SETRECOM' => '设置推荐',
  'ADMIN_DYNAMIC_SETSTATUS' => '审核',
  'ADMIN_DYNAMIC_WAIT' => '待审核列表',
  'ADMIN_DYNAMICCOM_DEL' => '删除',
  'ADMIN_DYNAMICCOM_INDEX' => '评论列表',
  'ADMIN_DYNAMICREPOT_INDEX' => '举报列表',
  'ADMIN_DYNAMICREPOT_SETSTATUS' => '处理',
  'ADMIN_DYNAMICREPOTCAT_ADD' => '添加',
  'ADMIN_DYNAMICREPOTCAT_ADDPOST' => '添加提交',
  'ADMIN_DYNAMICREPOTCAT_DEL' => '删除',
  'ADMIN_DYNAMICREPOTCAT_EDIT' => '编辑',
  'ADMIN_DYNAMICREPOTCAT_EDITPOST' => '编辑提交',
  'ADMIN_DYNAMICREPOTCAT_INDEX' => '举报类型',
  'ADMIN_DYNAMICREPOTCAT_LISTORDER' => '排序',
  'ADMIN_GIFT_ADD' => '添加',
  'ADMIN_GIFT_ADDPOST' => '添加提交',
  'ADMIN_GIFT_DEL' => '删除',
  'ADMIN_GIFT_EDIT' => '编辑',
  'ADMIN_GIFT_EDITPOST' => '编辑提交',
  'ADMIN_GIFT_INDEX' => '礼物管理',
  'ADMIN_GIFT_LISTORDER' => '排序',
  'ADMIN_GIFTRECORD_COINRECORD' => '礼物消费明细',
  'ADMIN_GIFTRECORD_VOTESRECORD' => '礼物收益明细',
  'ADMIN_GUIDE_ADD' => '添加',
  'ADMIN_GUIDE_ADDPOST' => '添加提交',
  'ADMIN_GUIDE_EDIT' => '编辑',
  'ADMIN_GUIDE_EDITPOST' => '编辑提交',
  'ADMIN_GUIDE_INDEX' => '管理',
  'ADMIN_GUIDE_SET' => '引导页',
  'ADMIN_HOBBY_ADD' => '添加',
  'ADMIN_HOBBY_ADDPOST' => '添加提交',
  'ADMIN_HOBBY_DEL' => '删除',
  'ADMIN_HOBBY_EDIT' => '编辑',
  'ADMIN_HOBBY_EDITPOST' => '编辑提交',
  'ADMIN_HOBBY_INDEX' => '兴趣管理',
  'ADMIN_HOBBY_LISTORDER' => '排序',
  'ADMIN_IMPRESSION_INDEX' => '标签管理',
  'ADMIN_LEVEL_ADD' => '添加',
  'ADMIN_LEVEL_ADDPOST' => '添加提交',
  'ADMIN_LEVEL_DEFAULT' => '等级管理',
  'ADMIN_LEVEL_DEL' => '删除',
  'ADMIN_LEVEL_EDIT' => '编辑',
  'ADMIN_LEVEL_EDITPOST' => '编辑提交',
  'ADMIN_LEVEL_INDEX' => '用户等级',
  'ADMIN_LEVEL_LISTORDER' => '排序',
  'ADMIN_LEVELANCHOR_ADD' => '添加',
  'ADMIN_LEVELANCHOR_ADDPOST' => '添加提交',
  'ADMIN_LEVELANCHOR_DEL' => '删除',
  'ADMIN_LEVELANCHOR_EDIT' => '编辑',
  'ADMIN_LEVELANCHOR_EDITPOST' => '编辑提交',
  'ADMIN_LEVELANCHOR_INDEX' => '陪玩等级',
  'ADMIN_LIVE_DEFAULT' => '聊天室',
  'ADMIN_LIVEAPPLY_INDEX' => '申请管理',
  'ADMIN_LIVEAPPLY_SETSTATUS' => '审核',
  'ADMIN_LIVEBG_ADD' => '添加',
  'ADMIN_LIVEBG_ADDPOST' => '添加提交',
  'ADMIN_LIVEBG_DEL' => '删除',
  'ADMIN_LIVEBG_EDIT' => '编辑',
  'ADMIN_LIVEBG_EDITPOST' => '编辑提交',
  'ADMIN_LIVEBG_INDEX' => '背景管理',
  'ADMIN_LIVEBG_LISTORDER' => '排序',
  'ADMIN_LIVEING_ADD' => '添加',
  'ADMIN_LIVEING_ADDPOST' => '添加提交',
  'ADMIN_LIVEING_DEL' => '关闭',
  'ADMIN_LIVEING_EDIT' => '编辑',
  'ADMIN_LIVEING_EDITPOST' => '编辑提交',
  'ADMIN_LIVEING_INDEX' => '列表',
  'ADMIN_LIVERECORD_INDEX' => '记录',
  'ADMIN_LOGINBONUS_EDIT' => '编辑',
  'ADMIN_LOGINBONUS_INDEX' => '签到奖励',
  'ADMIN_MANUAL_ADD' => '添加',
  'ADMIN_MANUAL_ADDPOST' => '添加提交',
  'ADMIN_MANUAL_INDEX' => '手动充值',
  'ADMIN_MENU_ADD' => '后台菜单添加',
  'ADMIN_MENU_ADDPOST' => '后台菜单添加提交保存',
  'ADMIN_MENU_DELETE' => '后台菜单删除',
  'ADMIN_MENU_EDIT' => '后台菜单编辑',
  'ADMIN_MENU_EDITPOST' => '后台菜单编辑提交保存',
  'ADMIN_MENU_GETACTIONS' => '导入新后台菜单',
  'ADMIN_MENU_INDEX' => '后台菜单',
  'ADMIN_MENU_LISTORDER' => '后台菜单排序',
  'ADMIN_MENU_LISTS' => '所有菜单',
  'ADMIN_ORDERCANCEL_ADD' => '添加',
  'ADMIN_ORDERCANCEL_ADDPOST' => '添加提交',
  'ADMIN_ORDERCANCEL_DEL' => '删除',
  'ADMIN_ORDERCANCEL_EDIT' => '编辑',
  'ADMIN_ORDERCANCEL_EDITPOST' => '编辑提交',
  'ADMIN_ORDERCANCEL_INDEX' => '取消原因',
  'ADMIN_ORDERCANCEL_LISTORDER' => '排序',
  'ADMIN_ORDERRECORD_INDEX' => '订单收支明细',
  'ADMIN_ORDERS_DEFAULT' => '订单管理',
  'ADMIN_ORDERS_INDEX' => '订单列表',
  'ADMIN_PHOTO_DEL' => '删除',
  'ADMIN_PHOTO_INDEX' => '相册管理',
  'ADMIN_PHOTO_SETSTATUS' => '审核',
  'ADMIN_PLUGIN_DEFAULT' => '插件中心',
  'ADMIN_PLUGIN_INDEX' => '插件列表',
  'ADMIN_PLUGIN_INSTALL' => '插件安装',
  'ADMIN_PLUGIN_SETTING' => '插件设置',
  'ADMIN_PLUGIN_SETTINGPOST' => '插件设置提交',
  'ADMIN_PLUGIN_TOGGLE' => '插件启用禁用',
  'ADMIN_PLUGIN_UNINSTALL' => '卸载插件',
  'ADMIN_PLUGIN_UPDATE' => '插件更新',
  'ADMIN_RBAC_AUTHORIZE' => '设置角色权限',
  'ADMIN_RBAC_AUTHORIZEPOST' => '角色授权提交',
  'ADMIN_RBAC_INDEX' => '角色管理',
  'ADMIN_RBAC_ROLEADD' => '添加角色',
  'ADMIN_RBAC_ROLEADDPOST' => '添加角色提交',
  'ADMIN_RBAC_ROLEDELETE' => '删除角色',
  'ADMIN_RBAC_ROLEEDIT' => '编辑角色',
  'ADMIN_RBAC_ROLEEDITPOST' => '编辑角色提交',
  'ADMIN_RECYCLEBIN_DELETE' => '回收站彻底删除',
  'ADMIN_RECYCLEBIN_INDEX' => '回收站',
  'ADMIN_RECYCLEBIN_RESTORE' => '回收站还原',
  'ADMIN_REFUND_INDEX' => '退款列表',
  'ADMIN_REFUNDCAT_INDEX' => '退款分类',
  'ADMIN_SETTING_CLEARCACHE' => '清除缓存',
  'ADMIN_SETTING_CONFIGPRI' => '私密设置',
  'ADMIN_SETTING_CONFIGPRIPOST' => '提交',
  'ADMIN_SETTING_DEFAULT' => '设置',
  'ADMIN_SETTING_PASSWORD' => '密码修改',
  'ADMIN_SETTING_PASSWORDPOST' => '密码修改提交',
  'ADMIN_SETTING_SITE' => '网站信息',
  'ADMIN_SETTING_SITEPOST' => '网站信息设置提交',
  'ADMIN_SETTING_UPLOAD' => '上传设置',
  'ADMIN_SETTING_UPLOADPOST' => '上传设置提交',
  'ADMIN_SKILL_ADD' => '添加',
  'ADMIN_SKILL_ADDPOST' => '添加提交',
  'ADMIN_SKILL_DEFAULT' => '技能管理',
  'ADMIN_SKILL_DEL' => '删除',
  'ADMIN_SKILL_EDIT' => '编辑',
  'ADMIN_SKILL_EDITPOST' => '编辑提交',
  'ADMIN_SKILL_INDEX' => '技能列表',
  'ADMIN_SKILL_LISTORDER' => '排序',
  'ADMIN_SKILLAUTH_INDEX' => '技能认证',
  'ADMIN_SKILLAUTH_SETSTATUS' => '审核',
  'ADMIN_SKILLCLASS_ADD' => '添加',
  'ADMIN_SKILLCLASS_ADDPOST' => '添加提交',
  'ADMIN_SKILLCLASS_DEL' => '删除',
  'ADMIN_SKILLCLASS_EDIT' => '编辑',
  'ADMIN_SKILLCLASS_EDITPOST' => '编辑提交',
  'ADMIN_SKILLCLASS_INDEX' => '技能分类',
  'ADMIN_SKILLCLASS_LISTORDER' => '排序',
  'ADMIN_SKILLCOIN_ADD' => 'tj',
  'ADMIN_SKILLCOIN_ADDPOST' => '添加提交',
  'ADMIN_SKILLCOIN_DEL' => '删除',
  'ADMIN_SKILLCOIN_EDIT' => '编辑',
  'ADMIN_SKILLCOIN_EDITPOST' => '编辑提交',
  'ADMIN_SKILLCOIN_INDEX' => '技能价格',
  'ADMIN_SKILLLEVEL_ADD' => '添加',
  'ADMIN_SKILLLEVEL_ADDPOST' => '添加提交',
  'ADMIN_SKILLLEVEL_DEL' => '删除',
  'ADMIN_SKILLLEVEL_EDIT' => '编辑',
  'ADMIN_SKILLLEVEL_EDITPOST' => '编辑提交',
  'ADMIN_SKILLLEVEL_INDEX' => '技能等级',
  'ADMIN_SKILLLEVEL_LISTORDER' => '排序',
  'ADMIN_SLIDE_ADD' => '添加幻灯片',
  'ADMIN_SLIDE_ADDPOST' => '添加幻灯片提交',
  'ADMIN_SLIDE_DELETE' => '删除幻灯片',
  'ADMIN_SLIDE_EDIT' => '编辑幻灯片',
  'ADMIN_SLIDE_EDITPOST' => '编辑幻灯片提交',
  'ADMIN_SLIDE_INDEX' => '幻灯片管理',
  'ADMIN_SLIDEITEM_ADD' => '幻灯片页面添加',
  'ADMIN_SLIDEITEM_ADDPOST' => '幻灯片页面添加提交',
  'ADMIN_SLIDEITEM_BAN' => '幻灯片页面隐藏',
  'ADMIN_SLIDEITEM_CANCELBAN' => '幻灯片页面显示',
  'ADMIN_SLIDEITEM_DELETE' => '幻灯片页面删除',
  'ADMIN_SLIDEITEM_EDIT' => '幻灯片页面编辑',
  'ADMIN_SLIDEITEM_EDITPOST' => '幻灯片页面编辑提交',
  'ADMIN_SLIDEITEM_INDEX' => '幻灯片页面列表',
  'ADMIN_SLIDEITEM_LISTORDER' => '幻灯片页面排序',
  'ADMIN_STORAGE_INDEX' => '文件存储',
  'ADMIN_STORAGE_SETTINGPOST' => '文件存储设置提交',
  'ADMIN_SYSNOTICE_ADD' => '添加',
  'ADMIN_SYSNOTICE_ADDPOST' => '添加提交',
  'ADMIN_SYSNOTICE_DEL' => '删除',
  'ADMIN_SYSNOTICE_INDEX' => '官方公告',
  'ADMIN_USER_ADD' => '管理员添加',
  'ADMIN_USER_ADDPOST' => '管理员添加提交',
  'ADMIN_USER_BAN' => '停用管理员',
  'ADMIN_USER_CANCELBAN' => '启用管理员',
  'ADMIN_USER_DEFAULT' => '管理组',
  'ADMIN_USER_DELETE' => '管理员删除',
  'ADMIN_USER_EDIT' => '管理员编辑',
  'ADMIN_USER_EDITPOST' => '管理员编辑提交',
  'ADMIN_USER_INDEX' => '管理员',
  'ADMIN_USER_USERINFO' => '个人信息',
  'ADMIN_USER_USERINFOPOST' => '管理员个人信息修改提交',
  'ADMIN_USERREPOT_DEL' => '删除',
  'ADMIN_USERREPOT_INDEX' => '举报列表',
  'ADMIN_USERREPOT_SETBANORDER' => '禁止接单',
  'ADMIN_USERREPOTCAT_ADD' => '添加',
  'ADMIN_USERREPOTCAT_ADDPOST' => '添加提交',
  'ADMIN_USERREPOTCAT_DEL' => '删除',
  'ADMIN_USERREPOTCAT_EDIT' => '编辑',
  'ADMIN_USERREPOTCAT_EDITPOST' => '编辑提交',
  'ADMIN_USERREPOTCAT_INDEX' => '举报类型',
  'ADMIN_USERREPOTCAT_LISTORDER' => '排序',
  'ADMIN_ZLIVE_DEFAULT' => '直播管理',
  'ADMIN_ZLIVEBAN_DEL' => '删除',
  'ADMIN_ZLIVEBAN_INDEX' => '禁播管理',
  'ADMIN_ZLIVECLASS_ADD' => '添加',
  'ADMIN_ZLIVECLASS_ADDPOST' => '添加提交',
  'ADMIN_ZLIVECLASS_DEL' => '删除',
  'ADMIN_ZLIVECLASS_EDIT' => '编辑',
  'ADMIN_ZLIVECLASS_EDITPOST' => '编辑提交',
  'ADMIN_ZLIVECLASS_INDEX' => '直播分类',
  'ADMIN_ZLIVECLASS_LISTORDER' => '排序',
  'ADMIN_ZLIVECOINRECORD_INDEX' => '直播收支明细',
  'ADMIN_ZLIVEING_ADD' => '添加',
  'ADMIN_ZLIVEING_ADDPOST' => '添加提交',
  'ADMIN_ZLIVEING_DEL' => '删除',
  'ADMIN_ZLIVEING_EDIT' => '编辑',
  'ADMIN_ZLIVEING_EDITPOST' => '编辑提交	',
  'ADMIN_ZLIVEING_INDEX' => '直播列表',
  'ADMIN_ZLIVERECORD_INDEX' => ' 直播记录',
  'ADMIN_ZLIVESHUT_DEL' => '删除',
  'ADMIN_ZLIVESHUT_INDEX' => '禁言管理',
  'ADMIN_ZMONITOR_FULL' => '大屏',
  'ADMIN_ZMONITOR_INDEX' => '直播监控',
  'ADMIN_ZMONITOR_STOP' => '关播',
  'PORTAL_ADMININDEX_DEFAULT' => '内容管理',
  'PORTAL_ADMINPAGE_ADD' => '添加',
  'PORTAL_ADMINPAGE_ADDPOST' => '添加提交',
  'PORTAL_ADMINPAGE_DELETE' => '删除',
  'PORTAL_ADMINPAGE_EDIT' => '编辑',
  'PORTAL_ADMINPAGE_EDITPOST' => '编辑提交',
  'PORTAL_ADMINPAGE_INDEX' => '页面管理',
  'USER_ADMINASSET_DELETE' => '删除文件',
  'USER_ADMINASSET_INDEX' => '资源管理',
  'USER_ADMININDEX_BAN' => '本站用户拉黑',
  'USER_ADMININDEX_CANCELBAN' => '本站用户启用',
  'USER_ADMININDEX_DEFAULT' => '用户管理',
  'USER_ADMININDEX_DEFAULT1' => '用户组',
  'USER_ADMININDEX_DEL' => '删除',
  'USER_ADMININDEX_INDEX' => '本站用户',
  'USER_ADMININDEX_SETHOST' => '设置主持人',
);