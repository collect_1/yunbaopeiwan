package com.yunbao.shortvideo;


import com.alibaba.android.arouter.launcher.ARouter;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.rtmp.TXLiveBase;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.CommonAppContext;
import com.yunbao.common.glide.ImgLoader;
import com.yunbao.common.utils.L;
import com.yunbao.im.utils.ImMessageUtil;

import cn.net.shoot.sharetracesdk.ShareTrace;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class AppContext extends CommonAppContext {

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
        boolean isDebug=isApkInDebug();
        L.setDeBug(isDebug);

        //腾讯云直播鉴权url
        String liveLicenceUrl = "http://license.vod2.myqcloud.com/license/v1/26aa72debb816c/TXLiveSDK.licence";
        //腾讯云直播鉴权key
        String liveLicenceKey = "512e9141ed2c8";
        TXLiveBase.getInstance().setLicence(this, liveLicenceUrl, liveLicenceKey);
        //初始化腾讯bugly
        CrashReport.initCrashReport(this);
        CrashReport.setAppVersion(this, CommonAppConfig.getInstance().getVersion());
        //初始化极光推送
        //ImPushUtil.getInstance().init(this);
        //初始化IM
        ImMessageUtil.getInstance().init();
        //初始化 ARouter
        if (isDebug) {
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);
        ShareTrace.init(this);

    }

    /*低内存的时候释放掉GLide的缓存*/
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        ImgLoader.clearMemory(this);
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        L.e("onTrimMemory==");
    }

}
