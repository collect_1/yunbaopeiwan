package com.yunbao.im.activity;


import android.os.Bundle;
import android.view.View;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.yunbao.common.activity.AbsActivity;
import com.yunbao.common.custom.refresh.RxRefreshView;
import com.yunbao.common.utils.WordUtil;
import com.yunbao.im.R;
import com.yunbao.im.adapter.SysMainAdapter;
import com.yunbao.im.bean.IMLiveBean;
import com.yunbao.im.utils.ImMessageUtil;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import java.util.List;
import io.reactivex.Observable;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class SystemMainActivity extends AbsActivity {
    private RxRefreshView<IMLiveBean> mRefreshView;
    private SysMainAdapter mSysMainAdapter;
    @Override
    protected int getLayoutId() {
        return R.layout.activity_system_main;
    }

    @Override
    protected void main(Bundle savedInstanceState) {
        super.main(savedInstanceState);
        EventBus.getDefault().register(this);
        setTitle(WordUtil.getString(R.string.im_system_main));
        mRefreshView = (RxRefreshView) findViewById(R.id.refreshView);
        mSysMainAdapter=new SysMainAdapter(null);
        mRefreshView.setDataListner(new RxRefreshView.DataListner<IMLiveBean>() {
            @Override
            public Observable<List<IMLiveBean>> loadData(int p) {
                return getData();
            }
            @Override
            public void compelete(List<IMLiveBean> data) {

            }
            @Override
            public void error(Throwable e) {
            }
        });
        mRefreshView.setAdapter(mSysMainAdapter);
        mRefreshView.setReclyViewSetting(RxRefreshView.ReclyViewSetting.createLinearSetting(this,10));
        mSysMainAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

            }
        });
        mRefreshView.initData();
    }
    private Observable<List<IMLiveBean>> getData() {
       return ImMessageUtil.getInstance().geSpatchList();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onRegisterLiveBean(IMLiveBean liveBean ){
        if(mSysMainAdapter!=null){
           mSysMainAdapter.addData(0,liveBean);
           mRefreshView.scrollPosition(0);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
