package com.yunbao.im.adapter;

import com.yunbao.common.adapter.base.BaseMutiRecyclerAdapter;
import com.yunbao.common.adapter.base.BaseReclyViewHolder;
import com.yunbao.common.utils.StringUtil;
import com.yunbao.im.R;
import com.yunbao.im.bean.IMLiveBean;

import java.util.List;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class SysMainAdapter extends BaseMutiRecyclerAdapter<IMLiveBean, BaseReclyViewHolder> {
    public SysMainAdapter(List<IMLiveBean> data) {
        super(data);
        addItemType(IMLiveBean.TYPE_AUTH_NOTICE,R.layout.item_recly_main_auth_notice);
    }

    @Override
    protected void convert(BaseReclyViewHolder helper, IMLiveBean item) {
        switch (helper.getItemViewType()){
            case IMLiveBean.TYPE_AUTH_NOTICE:
                convertAuthNotice(helper,item);
                break;
            default:
                break;
        }
    }

    private void convertAuthNotice(BaseReclyViewHolder helper, IMLiveBean item) {
        helper.setText(R.id.time,item.getTime() );
        helper.setText(R.id.tv_content, item.getTip_des());
    }

}
