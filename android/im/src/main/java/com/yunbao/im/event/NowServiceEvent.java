package com.yunbao.im.event;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class NowServiceEvent {
    private String orderId;
    private int receptStatus;

    public NowServiceEvent(String orderId, int receptStatus) {
        this.orderId = orderId;
        this.receptStatus = receptStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getReceptStatus() {
        return receptStatus;
    }

    public void setReceptStatus(int receptStatus) {
        this.receptStatus = receptStatus;
    }
}
