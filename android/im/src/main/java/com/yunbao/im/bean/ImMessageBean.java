package com.yunbao.im.bean;

import android.text.TextUtils;

import com.tencent.imsdk.TIMElem;
import com.tencent.imsdk.TIMElemType;
import com.tencent.imsdk.TIMMessage;
import com.tencent.imsdk.TIMSoundElem;
import com.tencent.imsdk.ext.message.TIMMessageExt;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.bean.OrderBean;




// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class ImMessageBean {

    public static final int TYPE_ORDER = 6;
    private String uid;//发消息的人的id
    private TIMMessage timRawMessage;//腾讯IM消息对象
    private int type;
    private long time;
    private String msgId;
    private OrderBean mOrderBean;

    public ImMessageBean(String uid, TIMMessage timRawMessage, int type, boolean fromSelf) {
        this.uid = uid;
        this.timRawMessage = timRawMessage;
        this.type = type;
        this.time = timRawMessage.timestamp() * 1000;
    }
    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
    public TIMMessage getTimRawMessage() {
        return timRawMessage;
    }


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public long getTime() {
        return time;
    }


    public OrderBean getOrderBean() {
        return mOrderBean;
    }

    public void setOrderBean(OrderBean orderBean) {
        mOrderBean = orderBean;
    }

    public String getMsgId() {
        return msgId;
    }

    public void setMsgId(String msgId) {
        this.msgId = msgId;
    }




}
