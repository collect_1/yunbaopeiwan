package com.yunbao.common.custom;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public SpacesItemDecoration(int space) {

        this.space = space;

    }

    @Override

    public void getItemOffsets(Rect outRect, View view,

                               RecyclerView parent, RecyclerView.State state) {

        outRect.right = space;



    }

}