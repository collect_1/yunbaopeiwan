package com.yunbao.common.custom;

import android.view.ViewTreeObserver;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class MyPreDrawFilter implements ViewTreeObserver.OnPreDrawListener {
    public MyPreDrawFilter(ZoomView zoomView) {
    }

    @Override
    public boolean onPreDraw() {
        return false;
    }
}
