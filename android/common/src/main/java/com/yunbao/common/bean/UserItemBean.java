package com.yunbao.common.bean;

import com.alibaba.fastjson.annotation.JSONField;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class UserItemBean {

    private int mId;
    private String mName;
    private String mThumb;
    private String mHref;
    private String mText;

    @JSONField(name = "id")
    public int getId() {
        return mId;
    }

    @JSONField(name = "id")
    public void setId(int id) {
        mId = id;
    }

    @JSONField(name = "name")
    public String getName() {
        return mName;
    }

    @JSONField(name = "name")
    public void setName(String name) {
        mName = name;
    }

    @JSONField(name = "thumb")
    public String getThumb() {
        return mThumb;
    }

    @JSONField(name = "thumb")
    public void setThumb(String thumb) {
        mThumb = thumb;
    }

    @JSONField(name = "href")
    public String getHref() {
        return mHref;
    }

    @JSONField(name = "href")
    public void setHref(String href) {
        mHref = href;
    }

    @JSONField(serialize = false)
    public String getText() {
        return mText;
    }

    @JSONField(serialize = false)
    public void setText(String text) {
        mText = text;
    }
}
