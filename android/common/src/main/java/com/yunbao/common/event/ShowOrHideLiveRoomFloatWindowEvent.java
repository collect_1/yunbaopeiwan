package com.yunbao.common.event;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class ShowOrHideLiveRoomFloatWindowEvent {
    private int showStatus;// 0 关闭 ；1显示； 2隐藏

    public ShowOrHideLiveRoomFloatWindowEvent(int showStatus) {
        this.showStatus = showStatus;
    }

    public int getShowStatus() {
        return showStatus;
    }

    public void setShowStatus(int showStatus) {
        this.showStatus = showStatus;
    }
}
