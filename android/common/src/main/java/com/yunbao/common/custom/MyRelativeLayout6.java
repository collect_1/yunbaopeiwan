package com.yunbao.common.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class MyRelativeLayout6 extends RelativeLayout {

    public MyRelativeLayout6(Context context) {
        super(context);
    }

    public MyRelativeLayout6(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyRelativeLayout6(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthSize = MeasureSpec.getSize(widthMeasureSpec);
        heightMeasureSpec = MeasureSpec.makeMeasureSpec(widthSize, MeasureSpec.EXACTLY);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }
}
