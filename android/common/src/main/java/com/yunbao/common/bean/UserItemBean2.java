package com.yunbao.common.bean;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class UserItemBean2 {

    private String mTitle;
    private List<UserItemBean> mList;

    @JSONField(name = "title")
    public String getTitle() {
        return mTitle;
    }
    @JSONField(name = "title")
    public void setTitle(String title) {
        mTitle = title;
    }
    @JSONField(name = "list")
    public List<UserItemBean> getList() {
        return mList;
    }
    @JSONField(name = "list")
    public void setList(List<UserItemBean> list) {
        mList = list;
    }
}
