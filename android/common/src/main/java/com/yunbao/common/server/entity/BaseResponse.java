package com.yunbao.common.server.entity;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class BaseResponse<T> {
    private int ret;
    private String msg;
    private Data<T> data;

    public int getRet() {

        return ret;
    }
    public void setRet(int ret) {
        this.ret = ret;
    }
    public String getMsg() {
        return msg;
    }
    public void setMsg(String msg) {
        this.msg = msg;
    }
    public Data<T> getData() {
        return data;
    }
    public void setData(Data data) {
        this.data = data;
    }
}
