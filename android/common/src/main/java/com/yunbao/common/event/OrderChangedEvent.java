package com.yunbao.common.event;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class OrderChangedEvent {
    private String mOrderId;
    private int status=-10;

    public OrderChangedEvent(String orderId) {
        mOrderId = orderId;
    }

    public String getOrderId(){
        return mOrderId;
    }


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


}
