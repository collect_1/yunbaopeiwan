package com.yunbao.common.utils;

import com.alibaba.android.arouter.launcher.ARouter;
import com.yunbao.common.Constants;


// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class RouteUtil {
    public static final String PATH_LAUNCHER = "/app/LauncherActivity";
    public static final String PATH_LOGIN_INVALID = "/main/LoginInvalidActivity";
    public static final String PATH_USER_HOME = "/main/UserHomeActivity";
    public static final String PATH_MAIN = "/main/MainActivity";
    public static final String PATH_COIN = "/main/MyCoinActivity";
    public static final String PATH_VIP = "/main/VipActivity";
    public static final String MAIN_ORDER_COMMENT = "/main/OrderCommentActivity";
    public static final String MAIN_ORDER_COMMENT_ANCHOR = "/main/OrderCommentActivity3";
    public static final String PUB_DYNAMIC = "/dynamics/PublishDynamicsActivity";
    public static final String PATH_CALL_SERVICE = "/im/CallService";
    public static final String PATH_CALL_ACTIVITY = "/im/CallActivity";
    public static final String PATH_SKILL_HOME = "/main/SkillHomeActivity";
    public static final String PATH_All_Skill = "/main/AllSkillActivity";
    public static final String PATH_ORDER_MAKE = "/main/OrderMakeActivity";
    public static final String PATH_ORDER_REFUND_DEAL = "/main/RefunDealActivity";

    public static final String PATH_LIVE_DISPATH_AUDIENCE = "/live/LiveDispatchAudienceActivity";
    public static final String PATH_LIVE_GOSSIP_AUDIENCE = "/live/LiveGossipAudienceActivity";
    public static final String PATH_LIVE_FRIEND_AUDIENCE = "/live/LiveFriendAudienceActivity";
    public static final String PATH_LIVE_SONG_AUDIENCE = "/live/LiveSongAudienceActivity";

    /**
     * 登录过期
     */

    public static void forwardLoginInvalid(String tip) {
        ARouter.getInstance().build(PATH_LOGIN_INVALID)
                .withString(Constants.TIP, tip)
                .navigation();
    }

    /**
     * 跳转到个人主页
     */

    public static void forwardUserHome(String toUid) {
        ARouter.getInstance().build(PATH_USER_HOME)
                .withString(Constants.TO_UID, toUid)
                .navigation();
    }


    /**
     * 跳转到充值页面
     */

    public static void forwardMyCoin() {
        ARouter.getInstance().build(PATH_COIN).navigation();
    }

}
