package com.yunbao.main.event;

import com.yunbao.main.bean.commit.DressingCommitBean;
// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class OpenDrawEvent {
    private DressingCommitBean mDressingCommitBean;

    public OpenDrawEvent(DressingCommitBean dressingCommitBean) {
        mDressingCommitBean = dressingCommitBean;
    }

    public DressingCommitBean getDressingCommitBean() {
        return mDressingCommitBean;
    }
}
