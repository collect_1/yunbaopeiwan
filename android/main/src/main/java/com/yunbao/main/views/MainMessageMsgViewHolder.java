package com.yunbao.main.views;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.yunbao.common.CommonAppConfig;
import com.yunbao.common.Constants;
import com.yunbao.common.utils.L;
import com.yunbao.common.utils.SpUtil;
import com.yunbao.common.views.AbsMainViewHolder;
import com.yunbao.im.activity.SystemMainActivity;

import com.yunbao.im.adapter.ImListAdapter;
import com.yunbao.im.bean.IMLiveBean;
import com.yunbao.im.bean.ImMessageBean;
import com.yunbao.im.bean.ImUserBean;
import com.yunbao.im.event.ImUserMsgEvent;
import com.yunbao.im.http.ImHttpConsts;
import com.yunbao.im.http.ImHttpUtil;
import com.yunbao.im.utils.ImMessageUtil;
import com.yunbao.main.R;
import com.yunbao.main.activity.OrderMsgActivity;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————
public class MainMessageMsgViewHolder extends AbsMainViewHolder implements View.OnClickListener{

    private static final String TAG = "MainMessageMsgViewHolde";
    private RecyclerView mRecyclerView;
    private ImListAdapter mAdapter;
    private TextView mOrderRedPoint;//订单消息的红点
    private View mBtnSystemMain;
    private TextView mSystemMsgPoint;//系统消息红点




    public MainMessageMsgViewHolder(Context context, ViewGroup parentView) {
        super(context, parentView);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.view_main_msg_msg;
    }

    @Override
    public void init() {
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        mAdapter = new ImListAdapter(mContext);
        mRecyclerView.setAdapter(mAdapter);
        View headView = mAdapter.getHeadView();
        mOrderRedPoint = headView.findViewById(R.id.red_point_order);
        mSystemMsgPoint = (TextView) headView.findViewById(R.id.red_point_main);
        headView.findViewById(R.id.btn_order).setOnClickListener(this);

        mBtnSystemMain = headView.findViewById(R.id.btn_system_main);
        mBtnSystemMain.setVisibility(View.VISIBLE);
        mBtnSystemMain.setOnClickListener(this);

        EventBus.getDefault().register(this);
        setSysMainRedPoinitVisible();
    }


    @Override
    public void loadData() {
        setSysMainRedPoinitVisible();
        ImUserBean orderMsg = ImMessageUtil.getInstance().getLastMsgInfo(Constants.IM_MSG_ADMIN);
        if(orderMsg!=null){
            if (mOrderRedPoint != null) {
                if (orderMsg.getUnReadCount() > 0) {
                    if (mOrderRedPoint.getVisibility() != View.VISIBLE) {
                        mOrderRedPoint.setVisibility(View.VISIBLE);
                    }
                    mOrderRedPoint.setText(String.valueOf(orderMsg.getUnReadCount()));
                } else {
                    if (mOrderRedPoint.getVisibility() == View.VISIBLE) {
                        mOrderRedPoint.setVisibility(View.INVISIBLE);
                    }
                }
            }
        }

    }

    /**
     * 是否显示系统消息红点
     */
    private void setSysMainRedPoinitVisible() {
        String  systemImUid = CommonAppConfig.getInstance().getConfig().getAdmin_dispatch();
        int unRead = ImMessageUtil.getInstance().getUnReadMsgCount(systemImUid);
        if(unRead > 0){
            mSystemMsgPoint.setVisibility(View.VISIBLE);
            mSystemMsgPoint.setText(String.valueOf(unRead));
        }else{
            mSystemMsgPoint.setVisibility(View.INVISIBLE);
        }
        ImMessageUtil.getInstance().refreshAllUnReadMsgCount();
    }


    @Override
    public void onClick(View v) {
        int i = v.getId();
         if (i == R.id.btn_order) {
            forwardOrderMsg();
        }else if (i == R.id.btn_system_main) {
            forwarMainSystem();
        }
    }

    /**
     * 系统消息
     */
    private void forwarMainSystem() {
        String admin=CommonAppConfig.getInstance().getConfig().getAdmin_dispatch();
        markRead(admin);
        SpUtil.getInstance().setBooleanValue(SpUtil.LIVE_BRO,false);
        if (mSystemMsgPoint != null && mSystemMsgPoint.getVisibility() == View.VISIBLE){
            mSystemMsgPoint.setVisibility(View.INVISIBLE);
        }
        startActivity(SystemMainActivity.class);
    }

    /**
     * 前往订单消息
     */
    private void forwardOrderMsg() {
        ImMessageUtil.getInstance().markAllMessagesAsRead(Constants.IM_MSG_ADMIN, true);
        if(mOrderRedPoint != null && mOrderRedPoint.getVisibility() == View.VISIBLE) {
           mOrderRedPoint.setVisibility(View.INVISIBLE);
        }
           OrderMsgActivity.forward(mContext);
    }


    /**
     * 忽略消息
     */
    public void ignoreUnReadCount(){
        ImMessageUtil.getInstance().markAllMessagesAsRead(Constants.IM_MSG_ADMIN, true);
        if(mOrderRedPoint != null && mOrderRedPoint.getVisibility() == View.VISIBLE) {
            mOrderRedPoint.setVisibility(View.INVISIBLE);
        }
        //系统消息
        String adminSys=CommonAppConfig.getInstance().getConfig().getAdmin_dispatch();
        markRead(adminSys);
        SpUtil.getInstance().setBooleanValue(SpUtil.LIVE_BRO,false);
        if (mSystemMsgPoint != null && mSystemMsgPoint.getVisibility() == View.VISIBLE){
            mSystemMsgPoint.setVisibility(View.INVISIBLE);
        }
    }


    private void markRead(String uid){
        if(TextUtils.isEmpty(uid)){
            return;
        }
        ImMessageUtil.getInstance().markAllMessagesAsRead(uid,true);
    }



    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onImUserMsgEvent(final ImUserMsgEvent e) {
        if (e == null) {
            return;
        }
        L.e(TAG,"onImUserMsgEvent--->"+e.getUnReadCount()+"---uid--->"+e.getUid());
        if (e.getType() == ImMessageBean.TYPE_ORDER) {
            //订单消息
            if (mOrderRedPoint != null) {
                if (mOrderRedPoint.getVisibility() != View.VISIBLE) {
                    mOrderRedPoint.setVisibility(View.VISIBLE);
                }
                int unReadCount=e.getUnReadCount();
                if(unReadCount<=0){
                   mOrderRedPoint.setText("1");
                }else{
                   mOrderRedPoint.setText(String.valueOf(e.getUnReadCount()));
                }
            }
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onIMLiveEvent(IMLiveBean e) {
        if(e!=null){
            setSysMainRedPoinitVisible();
        }
    }



    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        ImHttpUtil.cancel(ImHttpConsts.GET_SYSTEM_MESSAGE_LIST);
        super.onDestroy();
    }
}
