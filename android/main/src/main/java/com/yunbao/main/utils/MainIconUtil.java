package com.yunbao.main.utils;

import android.util.SparseIntArray;

import com.yunbao.common.Constants;
import com.yunbao.main.R;

// +———————————————————————————————————
// | Created by Yunbao
// +———————————————————————————————————
// | Copyright (c) 2013~2022 http://www.yunbaokj.com All rights reserved.
// +———————————————————————————————————
// | Author: https://gitee.com/yunbaokeji
// +———————————————————————————————————
// | Date: 2022-05-28
// +———————————————————————————————————

public class MainIconUtil {
    private static SparseIntArray sOnLineMap1;//在线类型图标
    private static SparseIntArray sCashTypeMap;//提现图片
    private static SparseIntArray sLiveTypeMap;//直播间类型图标

    static {
        sOnLineMap1 = new SparseIntArray();
        sOnLineMap1.put(Constants.LINE_TYPE_OFF, R.mipmap.o_home_line_off);
        sOnLineMap1.put(Constants.LINE_TYPE_DISTURB, R.mipmap.o_home_line_disturb);
        sOnLineMap1.put(Constants.LINE_TYPE_CHAT, R.mipmap.o_home_line_chat);
        sOnLineMap1.put(Constants.LINE_TYPE_ON, R.mipmap.o_home_line_on);


        sCashTypeMap = new SparseIntArray();
        sCashTypeMap.put(Constants.CASH_ACCOUNT_ALI, R.mipmap.icon_cash_ali);
        sCashTypeMap.put(Constants.CASH_ACCOUNT_BANK, R.mipmap.icon_cash_bank);


    }
    public static int getCashTypeIcon(int key) {
        return sCashTypeMap.get(key);
    }


}
