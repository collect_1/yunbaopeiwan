
![输入图片说明](/img/head.png)
### 项目资料、说明
开源源码：原生开发，可下载使用

搭建文档：辅助操作，程序可跑通【[点击查看后台搭建文档](https://shimo.im/docs/lYLHNRDCVvs6w1Al/)】【[点击查看安卓搭建文档](https://shimo.im/docs/W0dHIwJnHAszOPFw/)】

疑难辅助说明：常见问题解释，答疑解惑【见搭建文档最下方说明和本项目的下方评论】

查看演示：

后台地址：http://gityuedan.yunbaozb.com/admin   

后台用户名：admin    密码：visitor

安卓扫描下载：

![输入图片说明](/img/qr.png)


### 项目介绍

本开源项目由云豹科技官方自主研发，提供开源陪玩系统源码、搭建文档、疑难辅助说明，供大家参考、交流、使用
开源版程序主要围绕“陪玩下单接单”展开，如您需要更多专业功能、优质服务，可联系我们了解商用版本、运营版本。

### 获取数据库sql文件
请加QQ群获取。QQ群号为：159517417

[点击此处加群](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=y1jlpwoB902pi_SNIHg2SlQ04noJbM7E&authKey=esWseNVQOylxcERGQnctZ1gkFDHdbAYBiDvzcd50ir%2BzrMtUNJ8TqXJgMdBgsfoB&noverify=0&group_code=159517417)，或者QQ扫描下方二维码加群 


![输入图片说明](/img/qqqr.png)

### 联系我们

公司官网：http://www.yunbaokj.com/

客服电话：17662585037

![输入图片说明](/img/contact.png)

### 云豹科技介绍

![输入图片说明](/img/about.jpg)

陪玩源码| 游戏陪玩源码 | 陪玩app源码 | 约玩陪玩软件源码 | 陪玩系统源码